<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\MenuComment
 *
 * @property integer $id
 * @property integer $fb_user_id
 * @property integer $food_menu_id
 * @property string $content
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\FoodMenu $foodMenu
 * @property-read \App\FbUser $user
 * @method static \Illuminate\Database\Query\Builder|\App\MenuComment whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\MenuComment whereFbUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\MenuComment whereFoodMenuId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\MenuComment whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\MenuComment whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\MenuComment whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class MenuComment extends Model
{
    public function foodMenu()
    {
        return $this->belongsTo(FoodMenu::class, 'food_menu_id');
    }

    public function user()
    {
        return $this->belongsTo(FbUser::class, 'fb_user_id');
    }
}
