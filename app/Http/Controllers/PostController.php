<?php

namespace App\Http\Controllers;


use App\Post;
use Illuminate\Support\Facades\Input;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class PostController extends Controller
{
    public function index()
    {
        $word = Input::get('s', null);

        if (is_null($word)) {
            $paginator = Post::orderBy('id', 'desc')->paginate(100);
        } else {
            $paginator = Post::where('content', 'LIKE', '%' . $word . '%')
                ->orderBy('id', 'desc')
                ->paginate(100);
        }
        $postCollection = $paginator->getCollection();

        $result = fractal()->collection($postCollection, function ($post) {
            return [
                'id' => $post->fb_id,
                'content' => $post->content,
                'image_url' => $post->image_url
            ];
        })->paginateWith(new IlluminatePaginatorAdapter($paginator))->toArray();

        return response()->json($result);
    }

}