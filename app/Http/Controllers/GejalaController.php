<?php

namespace App\Http\Controllers;


use App\Gejala;
use Illuminate\Http\Request;

class GejalaController extends Controller
{
    public function index()
    {
        $gejalas = Gejala::get()->all();

        $data = fractal()
            ->collection($gejalas)
            ->transformWith(function ($g) {
                return [
                    'id' => $g->id,
                    'nama' => $g->nama
                ];
            })->toArray();

        return response()->json($data);
    }

    public function store(Request $request)
    {
        $newGejala = new Gejala();

        $newGejala->nama = $request->get('nama');
        $newGejala->save();

        return response()->isSuccessful();
    }

    public function delete($id)
    {
        $gejala = Gejala::find($id);

        $gejala->penyebabs()->detach();

        $gejala->delete();

        return response()->isSuccessful();
    }
}