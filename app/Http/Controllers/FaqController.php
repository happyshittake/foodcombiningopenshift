<?php

namespace App\Http\Controllers;


use App\Faq;

class FaqController extends Controller
{
    public function index()
    {
        $data = fractal()
            ->collection(Faq::with('category')->get()->all())
            ->transformWith(function ($faq) {
                return [
                    'id' => $faq['id'],
                    'pertanyaan' => $faq['pertanyaan'],
                    'jawaban' => $faq['jawaban'],
                    'kategori' => $faq['category']['name']
                ];
            })
            ->toArray();
        
        return response()->json($data);
    }
}