<?php

namespace App\Http\Controllers;


use App\Game;
use App\SwipeGameScore;
use Illuminate\Http\Request;

class GameController extends Controller
{
    public function index(Request $request)
    {
        $gamescards = Game::get();
        $time = $request->input('time');

        $cards = $gamescards->random(10)->all();

        $cardFractal = fractal()->collection($cards)->transformWith(function ($card) use ($time) {
            return [
                'imageUrl' => $card['image_url'],
                'answer' => $card['kategori'] == $time ? 'right' : 'left'
            ];
        })->toArray();

        return response()->json($cardFractal);
    }

    public function saveScore(Request $request)
    {
        SwipeGameScore::create([
            'fb_user_id' => $request->get('user_id'),
            'score' => $request->get('score')
        ]);

        return response()->json([
            'created' => true
        ]);
    }

    public function getAllScore()
    {
        $data = SwipeGameScore::with('user')->orderBy('score', 'desc')->get();

        $resp = fractal()->collection($data)
            ->transformWith(function (SwipeGameScore $score) {
                return [
                    'author' => is_null($score->user) ? 'deleted user' : $score->user->nama,
                    'score' => $score->score,
                    'tanggal' => $score->created_at->toFormattedDateString()
                ];
            })->toArray();

        return response()->json($resp);
    }
}