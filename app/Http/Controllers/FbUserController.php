<?php

namespace App\Http\Controllers;


use App\FbUser;
use Illuminate\Http\Request;

class FbUserController extends Controller
{
    private $fbUser;

    /**
     * FbUserController constructor.
     * @param FbUser $fbUser
     */
    public function __construct(FbUser $fbUser)
    {
        $this->fbUser = $fbUser;
    }

    public function register(Request $request)
    {
        $nama = $request->get('nama');
        $email = $request->get('email');
        $token = $request->get('token');

        $user = FbUser::where('email', $email)->first();

        if (!is_null($user)) {
            return response()->json([
                'found' => true,
                'id' => $user->id,
                'nama' => $user->nama,
                'email' => $user->email,
                'token' => $user->token,
                'admin' => (bool)$user->is_admin
            ], 200);
        }

        $newUser = new FbUser();

        $newUser->nama = $nama;
        $newUser->email = $email;
        $newUser->token = $token;
        $newUser->is_admin = false;

        $newUser->save();

        return response()->json([
            'found' => false,
            'id' => $newUser->id,
            'nama' => $newUser->nama,
            'email' => $newUser->email,
            'token' => $newUser->token,
            'admin' => (bool)$newUser->is_admin
        ], 201);
    }
}
