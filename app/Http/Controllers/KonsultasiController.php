<?php

namespace App\Http\Controllers;


use App\JawabanKonsultasi;
use App\PertanyaanKonsultasi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class KonsultasiController extends Controller
{
    public function getAllQuestion()
    {
        $questions = PertanyaanKonsultasi::orderBy('id', 'desc')->get();

        $data = fractal()
            ->collection($questions)
            ->transformWith(function (PertanyaanKonsultasi $q) {
                return [
                    'id' => $q->id,
                    'pertanyaan' => $q->content,
                    'jawaban' => is_null($q->jawaban) ? 'masi belum terjawab' : $q->jawaban,
                    'tanggal' => $q->created_at->toDateString(),
                    'author' => $q->user->nama
                ];
            })->toArray();

        return response()->json($data);
    }

    public function getQuestion($id)
    {
        $p = PertanyaanKonsultasi::where('id', $id)->first();

        $data = fractal()->item($p, function (PertanyaanKonsultasi $q) {
            return [
                'id' => $q->id,
                'pertanyaan' => $q->content,
                'jawaban' => is_null($q->jawaban) ? 'masi belum terjawab' : $q->jawaban,
                'tanggal' => $q->created_at->toDateString(),
                'author' => $q->user->nama
            ];
        })->toArray();

        return response()->json($data);
    }

    public function storeJawaban($id, Request $request)
    {
        $p = PertanyaanKonsultasi::with('jawaban')->where('id', $id)->first();

        if (is_null($p->jawaban)) {
            $j = new JawabanKonsultasi();
        } else {
            $j = JawabanKonsultasi::whereHas('pertanyaan', function ($q) use ($id) {
                $q->where('id', $id);
            })->first();
        }

        $j->content = $request->get('content');
        $j->pertanyaan_konsultasi_id = $id;

        $j->save();

        return response()->json([
            'created' => true,
            'id' => $j->id
        ]);
    }

    public function storePertanyaan(Request $request)
    {
        $newP = new PertanyaanKonsultasi();

        $newP->content = $request->get("pertanyaan");
        $newP->fb_user_id = $request->get('user');

        $newP->save();

        return response()->json([
            'created' => true,
            'id' => $newP->id
        ]);
    }
//untuk data user sendiri
    public function getUserQuestions()
    {
        $userId = Input::get('user');
        $q = PertanyaanKonsultasi::whereHas('user', function ($q) use ($userId) {
            $q->where('id', $userId);
        })->orderBy('id','asc')->get();

        $data = fractal()
            ->collection($q->all())
            ->transformWith(function (PertanyaanKonsultasi $d) {
                return [
                    'id' => $d->id,
                    'pertanyaan' => $d->content,
                    'jawaban' => is_null($d->jawaban) ? "masi belum terjawab" : $d->jawaban,
                    'tanggal' => $d->created_at->toDateString(),
                    'author' => $d->user->nama
                ];
            })->toArray();

        return response()->json($data);
    }
}
