<?php
/**
 * Created by PhpStorm.
 * User: ndjoe
 * Date: 05/12/16
 * Time: 18:57
 */

namespace App\Http\Controllers;


use App\Video;

class VideoController extends Controller
{
    public function index()
    {
        $videos = Video::get();

        $data = fractal()
            ->collection($videos)
            ->transformWith(function (Video $video) {
                return [
                    'id' => $video->id,
                    'url' => $video->url
                ];
            })->toArray();

        return response()->json($data);
    }
}