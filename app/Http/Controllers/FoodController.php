<?php

namespace App\Http\Controllers;


use App\FbUser;
use App\FoodDiary;
use App\FoodMenu;
use App\MenuComment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FoodController extends Controller
{
    public function getDiaryEventsInMonth()
    {
        $time = Input::get('time');
        $user_id = Input::get('user');
        $date = Carbon::parse($time);
        $diaries = FoodDiary::where(\DB::raw('MONTH(waktu)'), '=', $date->month)
            ->where(\DB::raw('YEAR(waktu)'), '=', $date->year)
            ->whereFbUserId($user_id)
            ->get();

        $diaries->load('food');

        $data = fractal()
            ->collection($diaries->all())
            ->transformWith(function ($d) {
                return [
                    'menu' => $d->food->name,
                    'kategori' => $d['keterangan'],
                    'time' => $d['waktu']->toDateTimeString()
                ];
            })->toArray();

        return response()->json($data);
    }

    public function getDiaryByDay()
    {
        $time = Input::get('time');
        $userId = Input::get('user');
        $date = Carbon::parse($time);
        $diaries = FoodDiary::where(\DB::raw('MONTH(waktu)'), '=', $date->month)
            ->where(\DB::raw('YEAR(waktu)'), '=', $date->year)
            ->where(\DB::raw('DAY(waktu)'), '=', $date->day)
            ->whereFbUserId($userId)
            ->get();

        $diaries->load('food');

        $data = fractal()
            ->collection($diaries->all())
            ->transformWith(function ($d) {
                return [
                    'menu' => $d->food->name,
                    'kategori' => $d['keterangan'],
                    'time' => $d['waktu']->toDateTimeString()
                ];
            })->toArray();

        return response()->json($data);
    }

    public function storeRecipe(Request $request)
    {
        $foodMenu = new FoodMenu();
        $foodMenu->name = $request->get('name');
        $foodMenu->deskripsi = $request->get('deskripsi');
        $foodMenu->is_fixed = true;
        $foodMenu->kategori = $request->get('kategori');
        $foodMenu->keterangan = $request->get('keterangan');
        $foodMenu->fb_user_id = $request->get('user');

        if ($request->hasFile('foto')) {
            $assetPath = $this->handleFile($request->file('foto'), $foodMenu->name);

            $foodMenu->foto = $assetPath;
        }

        $foodMenu->save();

        return response()->json([
            'created' => true,
            'id' => $foodMenu->id
        ]);
    }

    public function getMyRecipes(Request $request)
    {
        $userid = $request->get('userid');

        $menu = FoodMenu::where(function ($q) use ($userid) {
            $q->where('fb_user_id', $userid)->where('is_fixed', true);
        })->orderBy('id', 'desc')->get();

        $menu->load('user');

        $data = fractal()
            ->collection($menu)
            ->transformWith(function (FoodMenu $d) {
                return [
                    'id' => $d->id,
                    'nama' => $d->name,
                    'keterangan' => $d->keterangan,
                    'deskripsi' => $d->deskripsi,
                    'kategori' => $d->kategori,
                    'author' => is_null($d->user) ? 'admin' : $d->user->nama,
                    'image' => $d->foto
                ];
            })->toArray();

        return response()->json($data);
    }

    public function updateMyRecipe($id, Request $request)
    {
        $recipe = FoodMenu::where('id', $id)->first();

        $recipe->name = $request->get('nama');
        $recipe->deskripsi = $request->get('deskripsi');
        $recipe->keterangan = $request->get('keterangan');
        $recipe->kategori = $request->get('kategori');

        if ($request->hasFile('foto')) {
            $assetPath = $this->handleFile($request->file('foto'), $recipe->name);

            $recipe->foto = $assetPath;
        }

        $recipe->save();

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function menus()
    {
        $userid = Input::get('user');
        $user = FbUser::whereId($userid)->first();

        $menu = FoodMenu::whereIsFixed(true)->orderBy('id', 'desc')->get();

        $menu->load('user');

        $data = fractal()
            ->collection($menu)
            ->transformWith(function (FoodMenu $d) {
                return [
                    'id' => $d->id,
                    'nama' => $d->name,
                    'keterangan' => $d->keterangan,
                    'deskripsi' => $d->deskripsi,
                    'kategori' => $d->kategori,
                    'author' => is_null($d->user) ? 'admin' : $d->user->nama,
                    'image' => $d->foto
                ];
            })->toArray();

        return response()->json($data);
    }

    public function menu($id)
    {
        $foodMenu = FoodMenu::with('comments', 'comments.user', 'user')
            ->whereId($id)->first();

        $data = fractal()
            ->item($foodMenu, function (FoodMenu $d) {
                return [
                    'id' => $d->id,
                    'nama' => $d->name,
                    'keterangan' => $d->keterangan,
                    'deskripsi' => $d->deskripsi,
                    'kategori' => $d->kategori,
                    'author' => is_null($d->user) ? 'admin' : $d->user->nama,
                    'image' => $d->foto,
                    'comments' => $d->comments->toArray()
                ];
            })->toArray();

        return response()->json($data);
    }

    public function storeMenuComment($id, Request $request)
    {
        $comment = new MenuComment();

        $comment->fb_user_id = $request->get('user');
        $comment->content = $request->get('content');
        $comment->food_menu_id = $id;
        $comment->save();

        return response()->json([
            'created' => true,
            'id' => $comment->id
        ]);
    }

    public function store(Request $request)
    {
        $menu = $request->get('menu');
        $keterangan = $request->get('keterangan');
        $waktu = $request->get('waktu');
        $userId = $request->get('user');
        $foodMenu = FoodMenu::firstOrCreate([
            'name' => mb_strtolower($menu),
            'fb_user_id' => $userId
        ]);

        $diary = new FoodDiary();
        $diary->food_menu_id = $foodMenu->id;
        $diary->keterangan = $keterangan;
        $diary->fb_user_id = $userId;
        $diary->waktu = Carbon::parse($waktu);

        if ($diary->save()) {
            return response()->json([
                'created' => true,
                'diary_id' => $diary->id
            ]);
        }

        return response()->json([]);
    }

    public function delete($id)
    {
        $food = FoodMenu::find($id);

        if (!$food->id) {
            return response()->isNotFound();
        }

        $food->load('diaries');

        foreach ($food->diaries as $diary) {
            $diary->delete();
        }

        $food->load('comments');

        foreach ($food->comments as $comment) {
            $comment->delete();
        }

        $food->delete();

        return response()->json([
            'status' => 'success'
        ]);
    }

    private function handleFile(UploadedFile $uploadedFile, $nama)
    {
        $imageName = str_slug($nama) . '.' . Carbon::now()->timestamp . '.' . $uploadedFile->getClientOriginalExtension();

        $uploadedFile->move(
            base_path() . '/public/images/recipes/', $imageName
        );

        return url('images/recipes/' . $imageName);
    }

}
