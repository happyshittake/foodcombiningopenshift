<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\PertanyaanKonsultasi;
use Illuminate\Http\Request;

class ConsultationController extends Controller
{
    public function get()
    {
        $qs = PertanyaanKonsultasi::orderBy('id', 'desc')->get();

        $qs->load('user');

        $data = fractal()
            ->collection($qs->all())
            ->transformWith(function (PertanyaanKonsultasi $q) {
                return [
                    'id' => $q->id,
                    'pertanyaan' => $q->content,
                    'jawaban' => $q->jawaban,
                    'tanggal' => $q->created_at,
                    'author' => $q->user->nama
                ];
            })->toArray();

        return response()->json($data);
    }

    public function answer($id, Request $request)
    {
        $q = PertanyaanKonsultasi::where('id', $id)->first();

        $q->jawaban = $request->get('jawaban');

        $q->save();

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function detail($id)
    {
        $q = PertanyaanKonsultasi::where('id', $id)->first();

        $data = fractal()
            ->item($q, function (PertanyaanKonsultasi $q) {
                return [
                    'id' => $q->id,
                    'pertanyaan' => $q->content,
                    'jawaban' => $q->jawaban,
                    'author' => $q->user->nama
                ];
            });


        return response()->json($data);
    }
}
