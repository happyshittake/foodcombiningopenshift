<?php
/**
 * Created by PhpStorm.
 * User: ndjoe
 * Date: 05/12/16
 * Time: 18:11
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Video;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    public function index()
    {
        $videos = Video::get();

        $data = fractal()
            ->collection($videos)
            ->transformWith(function (Video $video) {
                return [
                    'id' => $video->id,
                    'url' => $video->url
                ];
            })->toArray();

        return response()->json($data);
    }

    public function delete($id)
    {
        $video = Video::findOrFail($id);
        $video->delete();

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function store(Request $request)
    {
        $video = new Video();
        $video->url = $request->get('url');

        $video->save();

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function update($id, Request $request)
    {
        $video = Video::findOrFail($id);
        $video->url = $request->get('url');

        $video->save();

        return response()->json([
            'status' => 'success'
        ]);
    }
}