<?php

namespace App\Http\Controllers\Admin;


use App\CategoryFaq;
use App\Faq;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    public function index()
    {
        $data = fractal()
            ->collection(Faq::with('category')->get()->all())
            ->transformWith(function ($faq) {
                return [
                    'id' => $faq['id'],
                    'pertanyaan' => $faq['pertanyaan'],
                    'jawaban' => $faq['jawaban'],
                    'kategori' => $faq['category']['name']
                ];
            })
            ->toArray();

        return response()->json($data);
    }

    public function store(Request $request)
    {
        $faq = new Faq();

        $faq->pertanyaan = $request->input('pertanyaan');
        $faq->jawaban = $request->input('jawaban');
        $category = CategoryFaq::firstOrCreate(['name' => $request->input('category_name')]);
        $faq->category_faq_id = $category->id;

        $faq->save();

        return response()->json([
            'created' => true
        ]);
    }


    public function update($id, Request $request)
    {
        $faq = Faq::where('id', $id)->first();

        $faq->pertanyaan = $request->input('pertanyaan');
        $faq->jawaban = $request->input('jawaban');
        $category = CategoryFaq::firstOrCreate(['name' => $request->input('category_name')]);
        $faq->category_faq_id = $category->id;

        $faq->save();

        return response()->json([
            'updated' => true
        ]);
    }


    public function delete($id)
    {
        $faq = Faq::where('id', $id)->first();

        $faq->delete();

        return response()->json([
            'deleted' => true
        ]);
    }

    public function getCategories()
    {
        $cats = CategoryFaq::get();

        $resp = fractal()
            ->collection($cats)
            ->transformWith(function (CategoryFaq $categoryFaq) {
                return [
                    'id' => $categoryFaq->id,
                    'name' => $categoryFaq->name
                ];
            })->toArray();

        return response()->json($resp);
    }
}