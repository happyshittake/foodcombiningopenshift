<?php

namespace App\Http\Controllers\Admin;

use App\Gejala;
use App\Http\Controllers\Controller;
use App\Penyebab;
use Illuminate\Http\Request;

class GejalaController extends Controller
{
    public function getGejalas()
    {
        $gejalas = Gejala::orderBy('id', 'desc')->get();

        $data = fractal()
            ->collection($gejalas)
            ->transformWith(function (Gejala $g) {
                return [
                    'id' => $g->id,
                    'nama' => $g->nama
                ];
            })->toArray();

        return response()->json($data);
    }

    public function getGejala($id)
    {
        $gejala = Gejala::where('id', $id)->first();

        $data = fractal()
            ->item($gejala, function (Gejala $g) {
                return [
                    'id' => $g->id,
                    'nama' => $g->nama
                ];
            })->toArray();

        return response()->json($data);
    }

    public function storeGejala(Request $request)
    {
        $gejala = new Gejala();

        $gejala->nama = $request->get('nama');

        $gejala->save();

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function updateGejala($id, Request $request)
    {
        $gejala = Gejala::where('id', $id)->first();

        $gejala->nama = $request->get('nama');

        $gejala->save();

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function deleteGejala($id)
    {
        $gejala = Gejala::where('id', $id)->first();

        $gejala->load('penyebabs');

        if ($gejala->penyebabs->count() > 0) {
            foreach ($gejala->penyebabs as $p) {
                $p->delete();
            }
        }

        $gejala->delete();

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function getPenyebabs()
    {
        $penyebabs = Penyebab::orderBy('id', 'desc')->get();

        $data = fractal()
            ->collection($penyebabs)
            ->transformWith(function (Penyebab $p) {
                return [
                    'id' => $p->id,
                    'nama' => $p->nama,
                    'solusi' => $p->solusi
                ];
            })->toArray();

        return response()->json($data);
    }

    public function getPenyebab($id)
    {
        $penyebab = Penyebab::where('id', $id)->first();

        $penyebab->load('gejalas');

        $data = fractal()
            ->item($penyebab, function (Penyebab $p) {

                return [
                    'id' => $p->id,
                    'nama' => $p->nama,
                    'solusi' => $p->solusi,
                    'gejalas' => $p->gejalas->toArray()
                ];
            })->toArray();

        return response()->json($data);
    }

    public function storePenyebab(Request $request)
    {
        $penyebab = new Penyebab();

        $penyebab->nama = $request->get('nama');
        $penyebab->solusi = $request->get('solusi');
        $penyebab->save();
        $arrIdx = explode(',', $request->get('gejalaids'));

        $penyebab->gejalas()->sync($arrIdx);

        return response()->json(
            [
                'status' => 'success'
            ]
        );
    }

    public function updatePenyebab($id, Request $request)
    {
        $penyebab = Penyebab::where('id', $id)->first();

        $penyebab->nama = $request->get('nama');
        $penyebab->solusi = $request->get('solusi');
        $penyebab->save();
        $arrIdx = explode(',', $request->get('gejalaids'));

        $penyebab->gejalas()->sync($arrIdx);

        return response()->json(
            [
                'status' => 'success'
            ]
        );
    }

    public function deletePenyebab($id)
    {
        $penyebab = Penyebab::where('id', $id)->first();

        $penyebab->delete();

        return response()->json(
            [
                'status' => 'success'
            ]
        );
    }
}
