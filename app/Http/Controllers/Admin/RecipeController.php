<?php

namespace App\Http\Controllers\Admin;

use App\FoodMenu;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class RecipeController extends Controller
{
    public function get()
    {
        $recipes = FoodMenu::orderBy('id', 'desc')->get();

        $recipes->load('user');

        $data = fractal()
            ->collection($recipes)
            ->transformWith(function (FoodMenu $menu) {
                return [
                    'id' => $menu->id,
                    'nama' => $menu->name,
                    'kategori' => $menu->kategori,
                    'deskripsi' => $menu->deskripsi,
                    'author' => is_null($menu->user) ? 'admin' : $menu->user->nama,
                    'keterangan' => $menu->keterangan,
                    'fixed' => $menu->is_fixed,
                    'image' => $menu->foto
                ];
            })->toArray();

        return response()->json($data);
    }

    public function store(Request $request)
    {
        $newRecipe = new FoodMenu();
        $newRecipe->name = $request->get('nama');
        $newRecipe->deskripsi = $request->get('deskripsi');
        $newRecipe->keterangan = $request->get('keterangan');
        $newRecipe->kategori = $request->get('kategori');
        $newRecipe->fb_user_id = 0;
        $newRecipe->is_fixed = $request->get('fixed') == "yes" ? true : false;

        if ($request->hasFile('foto')) {
            $assetPath = $this->handleFile($request->file('foto'), $newRecipe->name);

            $newRecipe->foto = $assetPath;
        }

        $newRecipe->save();

        return response()->json(
            [
                'status' => 'success'
            ]
        );
    }

    private function handleFile(UploadedFile $uploadedFile, $nama)
    {
        $imageName = str_slug($nama) . '.' . Carbon::now()->timestamp . '.' . $uploadedFile->getClientOriginalExtension();

        $uploadedFile->move(
            base_path() . '/public/images/recipes/', $imageName
        );

        return url('images/recipes/' . $imageName);
    }

    public function detail($id)
    {
        $recipe = FoodMenu::where('id', $id)->first();

        $recipe->load('user');

        $data = fractal()
            ->item($recipe, function (FoodMenu $menu) {
                return [
                    'id' => $menu->id,
                    'nama' => $menu->name,
                    'kategori' => $menu->kategori,
                    'deskripsi' => $menu->deskripsi,
                    'author' => is_null($menu->user) ? 'admin' : $menu->user->nama,
                    'fixed' => $menu->is_fixed,
                    'image' => $menu->foto
                ];
            })->toArray();

        return response()->json($data);
    }

    public function update($id, Request $request)
    {
        $recipe = FoodMenu::where('id', $id)->first();

        $recipe->name = $request->get('nama');
        $recipe->deskripsi = $request->get('deskripsi');
        $recipe->keterangan = $request->get('keterangan');
        $recipe->kategori = $request->get('kategori');
        $recipe->is_fixed = $request->get('fixed') == "yes" ? true : false;

        if ($request->hasFile('foto')) {
            $assetPath = $this->handleFile($request->file('foto'), $recipe->name);

            $recipe->foto = $assetPath;
        }

        $recipe->save();

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function delete($id)
    {
        $recipe = FoodMenu::where('id', $id)->first();

        $recipe->delete();

        return response()->json(
            [
                'status' => 'success'
            ]
        );
    }
}
