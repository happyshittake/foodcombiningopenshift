<?php

namespace App\Http\Controllers\Admin;

use App\FbUser;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class MemberController extends Controller
{
    public function get()
    {
        $members = FbUser::orderBy('id', 'desc')->get();

        $data = fractal()
            ->collection($members)
            ->transformWith(function (FbUser $user) {
                return [
                    'id' => $user->id,
                    'nama' => $user->nama,
                    'email' => $user->email,
                    'admin' => (bool)$user->is_admin
                ];
            })->toArray();

        return response()->json($data);
    }

    public function detail($id)
    {
        $member = FbUser::where('id', $id)->first();

        $data = fractal()
            ->item($member, function (FbUser $user) {
                return [
                    'id' => $user->id,
                    'nama' => $user->nama,
                    'email' => $user->email
                ];
            })->toArray();

        return response()->json($data);
    }

    public function promote($id)
    {
        $member = FbUser::where('id', $id)->first();

        $member->is_admin = true;

        $member->save();

        return response()->json(
            [
                'status' => 'success'
            ]
        );
    }

    public function depromote($id)
    {
        $member = FbUser::whereId($id)->first();

        $member->is_admin = false;

        $member->save();

        return response()->json(
            [
                'status' => 'success'
            ]
        );
    }

    public function update($id, Request $request)
    {
        $nama = $request->get('nama');

        $member = FbUser::where('id', $id)->first();

        $member->nama = $nama;

        $member->save();

        return response()->json(
            [
                'status' => 'success'
            ]
        );
    }

    public function delete($id)
    {
        $member = FbUser::where('id', $id)->first();

        $member->delete();

        return response()->json(
            [
                'status' => 'success'
            ]
        );
    }
}
