<?php
namespace App\Http\Controllers;


use App\Quiz;
use App\QuizScore;
use Illuminate\Http\Request;

class QuizController extends Controller
{
    public function index()
    {
        $quizzes = Quiz::get()->random(10);

        $data = fractal()
            ->collection($quizzes)
            ->transformWith(function ($q) {
                $choices = [
                    $q->choice_1,
                    $q->choice_2,
                    $q->choice_3,
                    $q->choice_4
                ];

                return [
                    'id' => $q->id,
                    'pertanyaan' => $q->pertanyaan,
                    'list_jawaban' => $choices,
                    'idx_jawaban' => $q->jawaban - 1
                ];
            })->toArray();

        return response()->json($data);
    }

    public function saveScore(Request $request)
    {
        $newScore = new QuizScore();

        $newScore->score = $request->get('score');
        $newScore->fb_user_id = $request->get('user_id');

        $newScore->save();

        return response()->json([
            'created' => true
        ]);
    }

    public function getAllScore()
    {
        $scores = QuizScore::orderBy('score', 'desc')->get();

        $scores->load('user');

        $data = fractal()
            ->collection($scores)
            ->transformWith(function (QuizScore $score) {
                return [
                    'author' => is_null($score->user) ? 'deleted user' : $score->user->nama,
                    'score' => $score->score,
                    'tanggal' => $score->created_at->toFormattedDateString()
                ];
            })->toArray();

        return response()->json($data);
    }
}