<?php

namespace App\Http\Controllers;


use App\Penyebab;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class PenyebabController extends Controller
{
    public function index()
    {
        $penyebab = Penyebab::get();

        $data = fractal()
            ->collection($penyebab)
            ->transformWith(function (Penyebab $p) {
                return [
                    'id' => $p->id,
                    'nama' => $p->nama,
                    'solusi' => $p->solusi
                ];
            })->toArray();

        return response()->json($data);
    }

    public function detail($id)
    {
        $penyebab = Penyebab::with('gejalas')->find($id);

        return response()->json($penyebab->toArray());
    }

    public function updateAturan($id)
    {
        $arrIdx = explode(',', Input::get('idx'));
        $p = Penyebab::find($id);

        $p->gejalas()->sync($arrIdx);

        return response()->isSuccessful();
    }

    public function storePenyebab(Request $request)
    {
        $nama = $request->get('nama');

        $newPenyebab = new Penyebab();
        $newPenyebab->nama = $nama;

        $newPenyebab->save();

        return response()->isSuccessful();
    }

    public function delete($id)
    {
        $penyebab = Penyebab::find($id);

        $penyebab->gejalas()->detach();

        $penyebab->delete();

        return response()->isSuccessful();
    }

    public function findPenyebabByGejalas()
    {
        $gejalas = Input::get('gejala');
        $arrG = explode(',', $gejalas);
        sort($arrG);
        $penyebabs = Penyebab::with('gejalas')->get();

        foreach ($penyebabs as $penyebab) {
            $pG = $penyebab->gejalas;
            $idxs = $pG->map(function ($item, $key) {
                return $item->id;
            })->toArray();
            sort($idxs);

            if ($arrG == $idxs) {
                return response()->json([
                    'found' => true,
                    'penyebab' => $penyebab->toArray()
                ]);
            }
        }

        return response()->json([
            'found' => false
        ]);
    }
}