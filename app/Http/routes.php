<?php

Route::group(['prefix' => 'api'], function () {
    Route::get('post', ['uses' => 'PostController@index']);
    Route::get('faq', ['uses' => 'FaqController@index']);
    Route::get('game', ['uses' => 'GameController@index']);
    Route::get('game/scores', ['uses' => 'GameController@getAllScore']);
    Route::post('game/scores', ['uses' => 'GameController@saveScore']);
    Route::get('diary', ['uses' => 'FoodController@getDiaryEventsInMonth']);
    Route::get('diary/day', ['uses' => 'FoodController@getDiaryByDay']);
    Route::post('diary', ['uses' => 'FoodController@store']);
    Route::get('menu', ['uses' => 'FoodController@menus']);
    Route::post('menu/my', ['uses' => 'FoodController@getMyRecipes']);
    Route::get('menu/{id}', ['uses' => 'FoodController@menu']);
    Route::post('menu/{id}/update', ['uses' => 'FoodController@updateMyRecipe']);
    Route::get('menu/{id}/delete', ['uses' => 'FoodController@delete']);
    Route::post('menu/recipe', ['uses' => 'FoodController@storeRecipe']);
    Route::post('menu', ['uses' => 'FoodController@store']);
    Route::post('menu/{id}/comment', ['uses' => 'FoodController@storeMenuComment']);
    Route::post('register', ['uses' => 'FbUserController@register']);
    Route::get('quiz', ['uses' => 'QuizController@index']);
    Route::get('konsultasi', ['uses' => 'KonsultasiController@getAllQuestion']);
    Route::get('konsultasi/user', ['uses' => 'KonsultasiController@getUserQuestions']);
    Route::get('konsultasi/{id}', ['uses' => 'KonsultasiController@getQuestion']);
    Route::post('konsultasi', ['uses' => 'KonsultasiController@storePertanyaan']);
    Route::post('konsultasi/{id}/answer', ['uses' => 'KonsultasiController@storeJawaban']);
    Route::get('gejala', ['uses' => 'GejalaController@index']);
    Route::post('gejala', ['uses' => 'GejalaController@store']);
    Route::get('gejala/{id}/delete', ['uses' => 'GejalaController@delete']);
    Route::get('penyebab', ['uses' => 'PenyebabController@index']);
    Route::post('penyebab', ['uses' => 'PenyebabController@storePenyebab']);
    Route::get('penyebab/{id}', ['uses' => 'PenyebabController@detail']);
    Route::get('penyebab/{id}/sync', ['uses' => 'PenyebabController@updateAturan']);
    Route::get('penyebab/{id}/delete', ['uses' => 'PenyebabController@delete']);
    Route::get('diagnosis', ['uses' => 'PenyebabController@findPenyebabByGejalas']);
    Route::post('quiz/score', ['uses' => 'QuizController@saveScore']);
    Route::get('quiz/score', ['uses' => 'QuizController@getAllScore']);
    Route::get('videos', ['uses' => 'VideoController@index']);


    Route::group(['prefix' => 'admin'], function () {
        Route::get('penyebabs', ['uses' => 'Admin\GejalaController@getPenyebabs']);
        Route::get('penyebabs/{id}', ['uses' => 'Admin\GejalaController@getPenyebab']);
        Route::post('penyebabs/{id}/update', ['uses' => 'Admin\GejalaController@updatePenyebab']);
        Route::post('penyebabs/store', ['uses' => 'Admin\GejalaController@storePenyebab']);
        Route::get('penyebabs/{id}/delete', ['uses' => 'Admin\GejalaController@deletePenyebab']);

        Route::get('gejalas', ['uses' => 'Admin\GejalaController@getGejalas']);
        Route::get('gejalas/{id}', ['uses' => 'Admin\GejalaController@getGejala']);
        Route::post('gejalas/{id}/update', ['uses' => 'Admin\GejalaController@updateGejala']);
        Route::post('gejalas/store', ['uses' => 'Admin\GejalaController@storeGejala']);
        Route::get('gejalas/{id}/delete', ['uses' => 'Admin\GejalaController@deleteGejala']);

        Route::get('consults', ['uses' => 'Admin\ConsultationController@get']);
        Route::get('consults/{id}', ['uses' => 'Admin\ConsultationController@detail']);
        Route::post('consults/{id}/answer', ['uses' => 'Admin\ConsultationController@answer']);

        Route::get('recipes', ['uses' => 'Admin\RecipeController@get']);
        Route::post('recipes/store', ['uses' => 'Admin\RecipeController@store']);
        Route::get('recipes/{id}', ['uses' => 'Admin\RecipeController@detail']);
        Route::post('recipes/{id}/update', ['uses' => 'Admin\RecipeController@update']);
        Route::get('recipes/{id}/delete', ['uses' => 'Admin\RecipeController@delete']);

        Route::get('members', ['uses' => 'Admin\MemberController@get']);
        Route::get('members/{id}', ['uses' => 'Admin\MemberController@detail']);
        Route::post('members/{id}/update', ['uses' => 'Admin\MemberController@update']);
        Route::get('members/{id}/delete', ['uses' => 'Admin\MemberController@delete']);
        Route::get('members/{id}/promote', ['uses' => 'Admin\MemberController@promote']);
        Route::get('members/{id}/depromote', ['uses' => 'Admin\MemberController@depromote']);

        Route::get('videos', ['uses' => 'Admin\VideoController@index']);
        Route::get('videos/{id}/delete', ['uses' => 'Admin\VideoController@delete']);
        Route::post('videos', ['uses' => 'Admin\VideoController@store']);
        Route::post('videos/{id}/update', ['uses' => 'Admin\VideoController@update']);
        Route::post('videos/{id}', ['uses' => 'Admin\VideoController@store']);

        Route::get('faqs', ['uses' => 'Admin\FaqController@index']);
        Route::post('faqs', ['uses' => 'Admin\FaqController@store']);
        Route::get('faqs/categories', ['uses' => 'Admin\FaqController@getCategories']);
        Route::post('faqs/{id}/update', ['uses' => 'Admin\FaqController@update']);
        Route::get('faqs/{id}/delete', ['uses' => 'Admin\FaqController@delete']);
    });
});
