<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PertanyaanKonsultasi
 *
 * @property integer $id
 * @property integer $fb_user_id
 * @property string $content
 * @property string $jawaban
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\FbUser $user
 * @method static \Illuminate\Database\Query\Builder|\App\PertanyaanKonsultasi whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PertanyaanKonsultasi whereFbUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PertanyaanKonsultasi whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PertanyaanKonsultasi whereJawaban($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PertanyaanKonsultasi whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PertanyaanKonsultasi whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PertanyaanKonsultasi extends Model
{
    public function user()
    {
        return $this->belongsTo(FbUser::class, 'fb_user_id');
    }
}
