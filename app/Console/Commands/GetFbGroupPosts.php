<?php

namespace App\Console\Commands;

use App\Post;
use Carbon\Carbon;
use Illuminate\Console\Command;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;

class GetFbGroupPosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fb:getposts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'get posts from facebook group';

    protected $fb;

    /**
     * Create a new command instance.
     *
     * @param LaravelFacebookSdk $fb
     */
    public function __construct(LaravelFacebookSdk $fb)
    {
        parent::__construct();

        $this->fb = $fb;
        $this->fb->setDefaultAccessToken('EAAHRiDY7mX0BAMuwXuH9uPn2MkWT5xzpnf4GVoV0SCAZCpyhNEvYkMfreeEmkEuN0s8ZC9KUzQ13T0xZBbg9DtW0AjktWsvWXSVwyPqPLnG17tsEOhyvwJ766WpTAZCT4UFqxi9lbXZBm0Km10Shx1W2hD5sjjh0ZD');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $resp = $this->fb->get(
            env('GROUP_FB_ID') . '/feed/?limit=500&from=' . Carbon::now()->toDateTimeString() . '&fields=message,picture'
        );

        $nodes = $resp->getGraphEdge();
        $nodeArr = array_reverse($nodes->all());

        foreach ($nodeArr as $item) {
            $newPost = $this->savePostGraphNodeToDB($item);

            $this->info('saved ' . $newPost->id);
        }
    }

    private function savePostGraphNodeToDB($graphNode)
    {
        $node = $graphNode->asArray();
        $newPost = Post::firstOrNew(['fb_id' => $node['id']]);

        if (array_key_exists('message', $node)) {
            $newPost->content = $node['message'];
        }
        if (array_key_exists('picture', $node)) {
            $newPost->image_url = $node['picture'];
        }
        $newPost->fb_id = $node['id'];

        $newPost->save();

        return $newPost;
    }
}
