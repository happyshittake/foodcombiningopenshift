<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\FoodMenu
 *
 * @property integer $id
 * @property integer $fb_user_id
 * @property string $name
 * @property string $kategori
 * @property string $deskripsi
 * @property string $keterangan
 * @property string $foto
 * @property boolean $is_fixed
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\FoodDiary[] $diaries
 * @property-read \App\FbUser $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\MenuComment[] $comments
 * @method static \Illuminate\Database\Query\Builder|\App\FoodMenu whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FoodMenu whereFbUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FoodMenu whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FoodMenu whereKategori($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FoodMenu whereDeskripsi($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FoodMenu whereKeterangan($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FoodMenu whereFoto($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FoodMenu whereIsFixed($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FoodMenu whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FoodMenu whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FoodMenu extends Model
{
    protected $fillable = [
        'name',
        'is_fixed',
        'fb_user_id'
    ];

    public function diaries()
    {
        return $this->hasMany(FoodDiary::class);
    }

    public function user()
    {
        return $this->belongsTo(FbUser::class, 'fb_user_id');
    }

    public function comments()
    {
        return $this->hasMany(MenuComment::class);
    }
}
