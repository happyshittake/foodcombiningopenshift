<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SwipeGameScore extends Model
{
    protected $fillable = [
        'score',
        'fb_user_id'
    ];

    public function user()
    {
        return $this->belongsTo(FbUser::class, 'fb_user_id');
    }
}
