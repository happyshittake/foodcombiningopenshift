<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\CategoryFaq
 *
 * @property integer $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Faq[] $faqs
 * @method static \Illuminate\Database\Query\Builder|\App\CategoryFaq whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CategoryFaq whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CategoryFaq whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\CategoryFaq whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CategoryFaq extends Model
{
    protected $fillable = [
        'name'
    ];

    public function faqs()
    {
        return $this->hasMany(Faq::class);
    }
}
