<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\FbUser
 *
 * @property integer $id
 * @property string $nama
 * @property string $email
 * @property string $token
 * @property boolean $is_admin
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\FoodDiary[] $diaries
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\FoodMenu[] $menus
 * @method static \Illuminate\Database\Query\Builder|\App\FbUser whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FbUser whereNama($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FbUser whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FbUser whereToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FbUser whereIsAdmin($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FbUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FbUser whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FbUser extends Model
{
    public function diaries()
    {
        return $this->hasMany(FoodDiary::class);
    }

    public function menus()
    {
        return $this->hasMany(FoodMenu::class);
    }
}
