<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\QuizScore
 *
 * @property integer $id
 * @property integer $fb_user_id
 * @property integer $score
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\FbUser $user
 * @method static \Illuminate\Database\Query\Builder|\App\QuizScore whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\QuizScore whereFbUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\QuizScore whereScore($value)
 * @method static \Illuminate\Database\Query\Builder|\App\QuizScore whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\QuizScore whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class QuizScore extends Model
{
    public function user()
    {
        return $this->belongsTo(FbUser::class, 'fb_user_id');
    }
}
