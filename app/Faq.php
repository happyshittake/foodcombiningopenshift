<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Faq
 *
 * @property integer $id
 * @property string $category_faq_id
 * @property string $pertanyaan
 * @property string $jawaban
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\CategoryFaq $category
 * @method static \Illuminate\Database\Query\Builder|\App\Faq whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Faq whereCategoryFaqId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Faq wherePertanyaan($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Faq whereJawaban($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Faq whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Faq whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Faq extends Model
{
    public function category()
    {
        return $this->belongsTo(CategoryFaq::class, 'category_faq_id');
    }
}
