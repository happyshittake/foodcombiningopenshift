<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Gejala
 *
 * @property integer $id
 * @property string $nama
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Penyebab[] $penyebabs
 * @method static \Illuminate\Database\Query\Builder|\App\Gejala whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Gejala whereNama($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Gejala whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Gejala whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Gejala extends Model
{
    public function penyebabs()
    {
        return $this->belongsToMany(Penyebab::class, 'aturans');
    }
}
