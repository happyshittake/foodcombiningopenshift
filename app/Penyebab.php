<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Penyebab
 *
 * @property integer $id
 * @property string $nama
 * @property string $solusi
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Gejala[] $gejalas
 * @method static \Illuminate\Database\Query\Builder|\App\Penyebab whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Penyebab whereNama($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Penyebab whereSolusi($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Penyebab whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Penyebab whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Penyebab extends Model
{
    public function gejalas()
    {
        return $this->belongsToMany(Gejala::class, 'aturans');
    }
}
