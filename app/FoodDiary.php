<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\FoodDiary
 *
 * @property integer $id
 * @property integer $food_menu_id
 * @property string $keterangan
 * @property \Carbon\Carbon $waktu
 * @property integer $fb_user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\FoodMenu $food
 * @property-read \App\FbUser $user
 * @method static \Illuminate\Database\Query\Builder|\App\FoodDiary whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FoodDiary whereFoodMenuId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FoodDiary whereKeterangan($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FoodDiary whereWaktu($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FoodDiary whereFbUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FoodDiary whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FoodDiary whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FoodDiary extends Model
{
    protected $dates = ['created_at', 'updated_at', 'waktu'];

    public function food()
    {
        return $this->belongsTo(FoodMenu::class, 'food_menu_id');
    }

    public function user()
    {
        return $this->belongsTo(FbUser::class, 'fb_user_id');
    }
}
