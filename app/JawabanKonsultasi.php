<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\JawabanKonsultasi
 *
 * @property integer $id
 * @property integer $pertanyaan_konsultasi_id
 * @property string $content
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\PertanyaanKonsultasi $pertanyaan
 * @method static \Illuminate\Database\Query\Builder|\App\JawabanKonsultasi whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\JawabanKonsultasi wherePertanyaanKonsultasiId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\JawabanKonsultasi whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\JawabanKonsultasi whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\JawabanKonsultasi whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class JawabanKonsultasi extends Model
{
    public function pertanyaan()
    {
        return $this->belongsTo(PertanyaanKonsultasi::class, 'pertanyaan_konsultasi_id');
    }
}
