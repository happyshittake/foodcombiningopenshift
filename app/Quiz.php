<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Quiz
 *
 * @property integer $id
 * @property string $pertanyaan
 * @property string $choice_1
 * @property string $choice_2
 * @property string $choice_3
 * @property string $choice_4
 * @property integer $jawaban
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Quiz whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Quiz wherePertanyaan($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Quiz whereChoice1($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Quiz whereChoice2($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Quiz whereChoice3($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Quiz whereChoice4($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Quiz whereJawaban($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Quiz whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Quiz whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Quiz extends Model
{
    //
}
