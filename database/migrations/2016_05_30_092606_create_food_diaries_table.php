<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodDiariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('food_diaries', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("food_menu_id");
            $table->string("keterangan");
            $table->timestamp("waktu");
            $table->unsignedInteger('fb_user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('food_diaries');
    }
}
