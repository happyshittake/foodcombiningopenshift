<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('food_menus', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('fb_user_id');
            $table->string("name");
            $table->string('kategori')->nullable();
            $table->text('deskripsi')->nullable();
            $table->string('keterangan')->nullable();
            $table->string('foto')->nullable();
            $table->boolean("is_fixed")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('food_menus');
    }
}
