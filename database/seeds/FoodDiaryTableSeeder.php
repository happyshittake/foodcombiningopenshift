<?php

use Illuminate\Database\Seeder;

class FoodDiaryTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menus = \App\FoodMenu::all();

        $diary1 = new \App\FoodDiary();
        $diary1->food_menu_id = $menus[0]->id;
        $diary1->keterangan = 'pagi';
        $diary1->waktu = \Carbon\Carbon::now()->addDay(1);
        $diary1->fb_user_id = 1;
        $diary1->save();

        $diary2 = new \App\FoodDiary();
        $diary2->food_menu_id = $menus[1]->id;
        $diary2->keterangan = 'siang';
        $diary2->waktu = \Carbon\Carbon::now()->addDay(2);
        $diary2->fb_user_id = 1;
        $diary2->save();

        $diary3 = new \App\FoodDiary();
        $diary3->food_menu_id = $menus[2]->id;
        $diary3->keterangan = 'malam';
        $diary3->waktu = \Carbon\Carbon::now()->addDay(3);
        $diary3->fb_user_id = 1;
        $diary3->save();
    }
}