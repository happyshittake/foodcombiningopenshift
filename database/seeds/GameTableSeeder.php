<?php

use App\Game;
use Illuminate\Database\Seeder;

class GameTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $imagenamespagi = [
            'apel.jpg',
            'pepaya.jpg',
            'nanas.jpg',
            'jeruk.jpg',
            'melon.jpg',
            'anggur.jpg',
            'naga.jpg',
            'kiwi.jpg',
            'strawberry.jpg',
            'sawo.jpg',
            'kesemek.jpg',
            'pisang.jpg',
            'mangga.jpg',
            'jambuair.jpg',
            'pir.jpg',
            'belimbing.jpg',
            'salak.jpg',
            'semangka.jpg',
            'jambubatu.jpg',
            'manggis.jpg',
            'durian.jpg',
            'nangka.jpg',
            'nasi.jpg',
            'sayurkacangmerah.jpg',
            'perkedelkentang.jpg',
            'perkedeljagung.jpg',
            'kentang.jpg',
            'jagung.jpg',
            'gadogado.jpg',
            'dagingsapi.jpg',
            'kepiting.jpg',
            'ikan.jpg',
            'cumicumi.jpg',
            'ayam.jpg',
            'tahu.jpg'
        ];

        $imagenamessiang = [
            'nasi.jpg',
            'sayurkacangmerah.jpg',
            'perkedelkentang.jpg',
            'perkedeljagung.jpg',
            'siomay.jpg',
            'kentang.jpg',
            'tahu.jpg',
            'sopsayur.jpg',
            'pangsitisisayur.jpg',
            'urapsayur.jpg',
            'gadogado.jpg',
            'apel.jpg',
            'pepaya.jpg',
            'nanas.jpg',
            'jeruk.jpg',
            'melon.jpg',
            'anggur.jpg',
            'naga.jpg',
            'kiwi.jpg',
            'strawberry.jpg',
            'sawo.jpg',
            'kesemek.jpg',
            'pisang.jpg',
            'mangga.jpg',
            'jambuair.jpg',
            'pir.jpg',
            'belimbing.jpg',
            'salak.jpg',
            'semangka.jpg',
            'jambubatu.jpg',
            'manggis.jpg',
            'durian.jpg',
            'nangka.jpg'
        ];

        $imagenamesmalam = [
            'ayamjahe.jpg',
            'muntahu.jpg',
            'tumissayur.jpg',
            'saladsayuran.jpg',
            'timikan.jpg',
            'saladmentimun.jpg',
            'pokcoytumis.jpg',
            'tahugoreng.jpg',
            'miekuahjamur.jpg',
            'apel.jpg',
            'pepaya.jpg',
            'nanas.jpg',
            'jeruk.jpg',
            'melon.jpg',
            'anggur.jpg',
            'naga.jpg',
            'kiwi.jpg',
            'strawberry.jpg',
            'sawo.jpg',
            'kesemek.jpg',
            'pisang.jpg',
            'mangga.jpg',
            'nasi.jpg'
        ];

        $basepath = '/images/cards/';

        foreach ($imagenamespagi as $nama) {
            $game = new Game();
            $game->image_url = $basepath . $nama;
            $game->kategori = 'pagi';
            $game->save();
        }

        foreach ($imagenamessiang as $image) {
            $gameSiang = new Game();
            $gameSiang->image_url = $basepath . $image;
            $gameSiang->kategori = 'siang';
            $gameSiang->save();
        }

        foreach ($imagenamesmalam as $imagemalam) {
            $gameMalam = new Game();
            $gameMalam->image_url = $basepath . $imagemalam;
            $gameMalam->kategori = 'malam';
            $gameMalam->save();
        }
    }
}