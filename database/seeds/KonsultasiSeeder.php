<?php

use Illuminate\Database\Seeder;

class KonsultasiSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pertanyaan = new \App\PertanyaanKonsultasi();
        $pertanyaan->fb_user_id = 1;
        $pertanyaan->content = 'pertanyaan 1';
        $pertanyaan->save();

        $jawaban = new \App\JawabanKonsultasi();
        $jawaban->content = 'jawaban 1';
        $jawaban->pertanyaan_konsultasi_id = 1;
        $jawaban->save();
    }
}