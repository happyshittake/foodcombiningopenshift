<?php

use Illuminate\Database\Seeder;

class FoodMenuTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jusAlpukat = new \App\FoodMenu();
        $jusAlpukat->name = 'jus alpukat';
        $jusAlpukat->is_fixed = true;
        $jusAlpukat->kategori = 'pagi';
        $jusAlpukat->fb_user_id = 1;
        $jusAlpukat->save();

        $batagor = new \App\FoodMenu();
        $batagor->name = 'batagor';
        $batagor->is_fixed = true;
        $batagor->kategori = 'pagi';
        $batagor->fb_user_id = 1;
        $batagor->save();

        $sopIga = new \App\FoodMenu();
        $sopIga->name = 'sop iga';
        $sopIga->fb_user_id = 1;
        $sopIga->kategori = 'pagi';
        $sopIga->is_fixed = true;
        $sopIga->save();
    }
}