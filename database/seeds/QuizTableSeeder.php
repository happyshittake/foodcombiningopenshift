<?php

use Illuminate\Database\Seeder;

class QuizTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $quizzes = [
            [
                'Apa itu Food Combining?',
                'A. Suatu cara mengatur asupan makanan yang diselar',
                'B. Suatu cara untuk diet, dengan makanan yang di k',
                'C. Suatu cara untuk menggabungkan suatu makanan.',
                'D. Semua benar',
                1
            ],
            [
                'Macam - macam kombinasi makanan yang serasi, kecua',
                'A. Protein dan Lemak',
                'B. Protein dan Pati',
                'C. Pati dan Lemak',
                'D. Gula dan Asam',
                2
            ],
            [
                'Macam - macam kombinasi makanan yang tidak serasi,',
                'A. Protein dan Pati',
                'B. Protein dan Asam',
                'C. Protein dan Lemak',
                'D. Pati dan Gula',
                3
            ],
            [
                'Kombinasi antara Daging dengan Tomat Segar termasu',
                'A. Protein dan Lemak',
                'B. Protein dan Gula',
                'C. Protein dan Pati',
                'D. Protein dan Asam',
                4
            ],
            [
                'Kombinasi antara Pasta dengan Jagung termasuk jeni',
                'A. Pati dan Pati', 'B. Pati dan Lemak',
                'C. Pati dan Asam', 'D. Pati dan Gula',
                1
            ],
            [
                'Kombinasi antara Tomat Segar dengan Yogurt  termas',
                'A. Asam dan Gula',
                'B. Asam dan Asam',
                'C. Asam dan Pati',
                'D. Asam dan Lemak',
                2
            ],
            [
                'Kombinasi antara Keju dengan Yogurt  termasuk jeni',
                'A. Asam dan Gula',
                'B. Lemak dan Gula',
                'C. Lemak dan Asam',
                'D. Asam dan Pati',
                3
            ],
            [
                'Kombinasi antara Madu dengan Yogurt termasuk jenis',
                'A. Asam dan Lemak',
                'B. Asam dan Asam',
                'C. Gula dan Lemak',
                'D. Gula dan Asam',
                4
            ],
            [
                'Kombinasi antara Jagung dengan Keju termasuk jenis',
                'A. Pati dan Lemak',
                'B. Pati dan Gula',
                'C. Pati dan Asam',
                'D. Pati dan Pati',
                1
            ],
            [
                'Kombinasi antara Serelia dengan Kacang - kacangan ',
                'A. Pati dan Pati',
                'B. Pati dan Protein',
                'C. Pati dan Asam',
                'D. Pati dan Lemak',
                2
            ],
            [
                'Apa itu Detoksifikasi?',
                'A. Proses penimbunan racun dalam tubuh.',
                'B. Proses pembuatan racun dalam tubuh.',
                'C. Proses pengeluaran zat-zat bersifat toksin atau',
                'D. Proses penambahan racun dalam tubuh.',
                3
            ],
            [
                'Minuman yang wajib diminum pada pagi hari setelah ',
                'A. Es The',
                'B. Minuman bersoda',
                'C. Minumam Beralkohol',
                'D. Jeniper (Jeruk Nipis Peras Hangat)',
                4
            ],
            [
                'Food Combining adalah pola makan sehat tertua di d',
                'A. 1947', 'B. 1974', 'C. 1794', 'D. 1749',
                1
            ],
            [
                'Pada pukul berapa fase penyerapan berlangsung?',
                'A. 20.00 - 02.00', 'B. 20.00 - 04.00', 'C. 20.00 - 06.00', 'D. 20.00 - 08.00',
                2
            ],
            [
                'Manakah yang termasuk makanan rendah energi dalam ',
                'A. Gula pasir', 'B. Makanan Kaleng', 'C. Kentang', 'D. Fastfood',
                3
            ],
            [
                'Pada pukul berapa fase pencernaan berlangsung?', 'A. 12.00 - 04.00', 'B. 12.00 - 18.00',
                'C. 12.00 - 02.00',
                'D. 12.00 - 20.00',
                4
            ],
            [
                'Pada pukul berapa fase pembuangan berlangsung?', 'A. 04.00 - 12.00', 'B. 04.00 - 23.00',
                'C. 04.00 - 08.00',
                'D. 04.00 - 20.00',
                1
            ],
            [
                'Buah apa saja yang tidak boleh dimakan saat sarapa',
                'A. Durian', 'B. Semangka', 'C. Cempedak', 'D. Nangka',
                2
            ],
            [
                'Apa saja manfaat mengikuti food combining, kecuali',
                'A. Makan lebih proporsional',
                'B. Penyembuhan dan Perawatan Penyakit',
                'C. Memiliki waktu luang yang banyak',
                'D. Berat Badan Ideal',
                3
            ],
            [
                'Pada food combining kita mengincar kondisi Homeost',
                'A. Kondisi tidak ideal dalam tubuh saat seluruh fu',
                'B. Kondisi tidak ideal dalam tubuh saat seluruh fu',
                'C. Kondisi food combining saat tubuh mereka merasa',
                'D. Kondisi ideal dalam tubuh saat seluruh fungsi b',
                4
            ],
            [
                'Mengapa protein hewani jangan dikonsumsi terlalu s',
                'A. Karena penguraian protein hewani ke bentuk asam',
                'B. Karena penguraian protein hewani berbenturan de',
                'C. Karena protein hewani dapat sangat cepat disera',
                'D. Karena penguraian protein hewani dapat diserap ',
                1
            ],
            [
                'Pada pukul berapa sarapan buah dilakukan bagi pemu',
                'A. 06.00 - 20.00', 'B. 06.00 - 11.00', 'C. 06.00 - 15.00', 'D. 06.00 - 13.00',
                2
            ],
            [
                'Kombinasi mana yang ideal untuk makan siang?',
                'A. Protein - Buah', 'B. Buah - Sayur', 'C. Protein - Sayuran', 'D. Protein - Pati',
                3
            ],
            [
                'Manakah yang termasuk minumam rendah energi dalam ',
                'A. Susu Sapi',
                'B. Alkohol',
                'C. Softdrink',
                'D. Jus Buah',
                4
            ]
        ];

        for ($i = 0; $i < sizeof($quizzes); $i++) {
            $newQuiz = new \App\Quiz;

            $newQuiz->pertanyaan = $quizzes[$i][0];
            $newQuiz->choice_1 = $quizzes[$i][1];
            $newQuiz->choice_2 = $quizzes[$i][2];
            $newQuiz->choice_3 = $quizzes[$i][3];
            $newQuiz->choice_4 = $quizzes[$i][4];
            $newQuiz->jawaban = $quizzes[$i][5];

            $newQuiz->save();
        }
    }
}