<?php

class CategoryFaqSeeder extends \Illuminate\Database\Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrayCat = [
            'DASAR FOOD COMBINING',
            'CARA MELAKUKAN FOOD COMBINING',
            'TATA TERTIB MAKAN',
            'CONTOH MENU FOOD COMBINING',
            'AIR',
            'PERTANYAAN UMUM',
            'PERTANYAAN SEPUTAR MAKANAN',
            'PERTANYAAN SEPUTAR PENYAKIT'
        ];

        foreach ($arrayCat as $cat) {
            $catObj = new \App\CategoryFaq();

            $catObj->name = $cat;
            $catObj->save();
        }
    }
}