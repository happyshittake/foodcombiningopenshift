<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(FbUsersTableSeeder::class);
        $this->call(CategoryFaqSeeder::class);
        $this->call(FaqTableSeeder::class);
        $this->call(GameTableSeeder::class);
        $this->call(FoodMenuTableSeeder::class);
        $this->call(FoodDiaryTableSeeder::class);
        $this->call(QuizTableSeeder::class);
        $this->call(KonsultasiSeeder::class);
        $this->call(DiagnosisSeeder::class);
    }
}
