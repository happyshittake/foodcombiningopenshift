<?php

use Illuminate\Database\Seeder;

class FbUsersTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $masterUser = new \App\FbUser();

        $masterUser->email = 'master@master.com';
        $masterUser->token = '1234';
        $masterUser->is_admin = true;
        $masterUser->nama = 'master';
        
        $masterUser->save();
    }
}