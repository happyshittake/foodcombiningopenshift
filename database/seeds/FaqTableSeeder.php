<?php

use App\Faq;
use Illuminate\Database\Seeder;

class FaqTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrayFaq = [
            [
                1,
                'Apa sih Food Combining itu ?',
                'Menurut Erikar Lebang : Food Combining adalah pengaturan pola makan yang mengacu kepada sistem cerna tubuh terutama sistem cerna. Pola makan ini lebih mengacu ke mekanisme pencernaan alamiah tubuh dalam menerima jenis makanan yang serasi sehingga tubuh dapat memproses semua itu dengan baik dan mendapatkan hasil secara maksimal ? Erikar Lebang (Food Combining itu Gampang dan Food Combining di Bulan Ramadhan). Menurut Andang Gunawan : Food Combining adalah metode pengaturan asupan makanan yang diselaraskan dengan mekanisme alamiah tubuh, khususnya yang berhubungan dengan sistem pencernaan. Efek pola makan ini meminimalkan jumlah penumpukan sisa makanan dan metabolisme sehingga fungsi pencernaan dan penyerrapan zat  makanan menjadi lancar dan pemakaian energi tubuh juga lebih efisien ? Andang Gunawan (Food Combining, Kombinasi Makanan Serasi).'
            ],
            [
                1,
                'Apa itu Homeostatis ?',
                'Karena kita mengincar kondisi yang bernama Homeostatis'
            ],
            [
                1,
                'Bagaimana bila pH tubuh kita cenderung ke asam ?',
                'Tubuh yang terlalu berat ke sisi asam, riskan mengundang berbagai macam penyakit. Dalam kondisi asam tubuh bagaikan magnet bagi penyakit. Organ tubuh akan berada dalam kondisi tidak prima serta cenderung rusak perlahan-lahan.'
            ],
            [
                1,
                'Bagaimana cara termudah tubuh mencapai kondisi Homeostatis ?',
                'Food Combining adalah salah satu cara termudah untuk mencapai kondisi tersebut'
            ],
            [
                2,
                'Bagaimana caranya melakukan Food Combining ?',
                'Food Combining memperkenalkan pola makan yang berbasis pada 3 hal sederhana, yaitu : 1. Apa yang kita makan 2. Waktu makan 3. Bagaimana memakannya'
            ],
            [
                2,
                'Apa yang kita makan ?',
                'Secara sederhana unsur makanan yang kita kenal adalah Pati, Protein, Sayuran dan Buah'
            ],
            [
                2,
                'Apa itu pati ?',
                'Pati adalah salah satu bentuk hidrat arang pemberi tenaga serta rasa kenyang yang lumayan cepat. Manusia membutuhkan pati saat dimakan tidak memberatkan kerja sistem cerna. Pati yang tergolong baik masih mengandung vitamin, serat, enzim, mineral dan substansi penting lain yang bisa dimanfaatkan tubu'
            ],
            [
                2,
                'Apa itu Protein ?',
                'Protein dikenal sebagai pembentuk sel-sel baru tubuh. Protein terbagi atas Protein Hewani dalam bentuk daging-dagingan, susu dan telur dan Protein Nabati dalam bentuk kacang-kacangan dan polong-polongan.'
            ],
            [
                2,
                'Mengapa protein hewani jangan dikonsumsi terlalu sering ? ',
                'Karena penguraian protein hewani ke dalam bentuk asam amino agar bisa diserap tubuh berlangsung lama dan memberatkan kerja sistem cerna. Penguraian ini juga menyedot energi yang seharusnya di alokasikan secara kolektif untuk menjaga keseimbangan tubuh. Asam amino pun mudah rusak karena harus diprose'
            ],
            [
                2,
                'Apa itu sayuran ?',
                'Sayuran adalah unsur makanan yang amat berguna sebagai pembentuk basa. Apabila di konsumsi secara benar sayuran akan mampu menetralkan pH dan menciptakan kondisi homeostatis. Sayuran kaya akan air dan mengonsumsi sayuran dalam keadaan segar mampu membantu mengisi kebutuhan tubuh akan asupan cairan h'
            ],
            [
                2,
                'Mengapa sayuran harus dikonsumsi dalam keadaan segar ?',
                'Karena sayuran yang melewati sesi pemanasan yang akan merusak cadangan air, enzim, nutrisi dan mineral yang terkandung didalamnya. Pada suhu 400 derajat celcius sewaktu pemanasan enzim akan mati.'
            ],
            [
                2,
                'Berapa lama sebaiknya menyimpan sayuran di kulkas ?',
                'Kalau bicara kualitas maksimal, sejatinya sayuran segar yang baik, sebaiknya tidak melebihi 3 hari waktu simpan dalam lemari pendingin.'
            ],
            [
                2,
                'Apa itu Buah ?',
                'Buah merupakan kelompok makanan penyumbang air, enzim, karbohidrat, serat, vitamin dan mineral. Konsumsi buah dengan benar akan memberikan sifat basa pada tubuh. Buah memiliki kandungan serat dan enzim cerna yg mampu bantu tubuh hilangkan tumpukan makanan dari usus besar.'
            ],
            [
                2,
                'Mengapa cara mengonsumsi buah harus benar ?',
                'Karena buah mengandung fruktosa yang gunanya memasok energi yang cepat bagi tubuh, oleh karenanya harus di konsumsi dengan cermat dan tepat, karena fruktosa bersifat merusak protein dan lemak.'
            ],
            [
                2,
                'Apakah buah dan sayur dapat dikombinasikan ?',
                'Tidak, karena serat buah cenderung lunak dan tidak serasi saat dipadukan dengan serat sayuran yang lebih keras terutama yang mempunyai sistem cerna sensitif. Sejauh ini paduan buah dan sayur yang paling aman adalah Wortel dan Apel.'
            ],
            [
                2,
                'Catatan Konsumsi Sayur dan Buah ?',
                '1 Konsumsi secara segar tanpa dimasak. 2 Dapatkan organik apabila memungkinkan. 3 Membersihkan dengan cara mencuci dibawah air mengalir selama dua sampai tiga menit. Yang lebih efektif rendam dalam tetesan cuka apel atau sabun pencuci buah dan sayur yang dijual di apotek atau toko organik. 4 Mengupa'
            ],
            [
                2,
                'Bagaimana dengan waktu makan ?',
                'Food Combing mengacu kepada ritme biologis dalam mengatur waktu dan jenis makanan yang tepat sesuai kebutuhan tubuh. Ritme biologis dikenal juga dengan Ritme Sirkadian'
            ],
            [
                2,
                'Bagaimana Ritme Sirkadian bekerja ?',
                'Ritme Sirkadian terbagi atas 3 waktu atau 3 Fase, yaitu : Pertama, 12.00 sampai 20.00 Waktu Cerna. Sistem Cerna berlaku akif dalam menerima makanan masuk. Fase ini sejalan dengan waktu makan siang, kudapan sore dan makan malam. Kedua, 20.00 sampai 04.00 Waktu Penyerapan. Pada fase ini tubuh memanfaa'
            ],
            [
                2,
                'Bagaimana jika tidak makan malam atau masih mengonsumsi makan malam di waktu penyerapan ?',
                'Butuh energi sangat besar dan proses rumit pada fase ini. Sehingga secara alamiah manusia memasuki waktu tidur. Mengganggu fase ini dengan konsumsi makanan atau tidak makan atau tidak tidur akan mengganggu alokasi energi tersebut. Sehingga akan membuat kerusakan kesehatan jangka pendek atau panjang.'
            ],
            [
                2,
                'Kemudian bagaimana dengan basis ke 3 Food Combining yaitu bagaimana memakannya ?',
                'Bagaimana memakannya adalah sesuai dengan Ritme Sirkadian.'
            ],
            [
                2,
                'Jadi bagaimana pengaturan pola makan berdasarkan Ritme Sirkadian ?',
                'Pada pagi hari saat alokasi tubuh untuk waktu pembuangan, makanan yang lebih ringan dan mudah serap oleh tubuh. Makanan dan kudapan yang bersifat lebih padat dialokasikan pada waktu siang, sore dan malam.'
            ],
            [
                2,
                'Apa yang diminum pada waktu baru bangun tidur ?',
                'Tepat saat bangun pagi ketika mulut masih kering, tubuh baru saja selesai bekerja keras bermetabolisme. Hadiahi dengan segelas air hangat berperasan lemon/jeruk nipis.'
            ],
            [
                2,
                'Apa fungsi air hangat ?',
                'Air hangat mengaktifkan beragam enzim dalam tubuh sekaligus bantu bersihkan lendir sisa metabolisme di sepanjang sistem cerna.'
            ],
            [
                2,
                'Apa fungsi perasan lemon atau jeruk nipis ?',
                'Pemberian perasan lemon atau jeruk nipis selain memberikan sifat basa pada air yang masuk tersebut, juga memperkuat daya bersih lendir pada air. Sekaligus memberikan tonik bagi liver. Organ ini bekerja ekstra keras saat tidur. pagi hari tonik menjadi ‘hadiah’ baginya.'
            ],
            [
                2,
                'Mengapa Liver ?',
                'Karena Liver yang kuat menjamin tubuh Anda sehat. Penyakit sulit mampir, penyakit yang ada pun mudah digerus oleh pertahanan tubuh.  Air hangat berlemon ini tidak berbahaya bagi lambung.'
            ],
            [
                2,
                'Mengapa tidak berbahaya bagi lambung ?',
                'Karena asam lambung tidak terstimulasi oleh sifat basa bawaan lemon. Konsep ini air hangat lemon secara tidak langsung juga memudahkan kerja organ vital lainnya, ginjal. Dalam fase sekresi tubuh.'
            ],
            [
                2,
                'Bagaimana membuat perasan jeruk nipis dengan air hangat ?',
                'Tips dari Arnaldo Wenas Share sedikit, mengenai cara nyiapin Air hangat perasan lemon atau jeruk nipis. 1. siapin air hangat terlebih dahulu di gelas 2. baru peres atau tuang perasan lemon atau jeruk nipisnya hal simple sih, tapi hasilnya bisa beda, soalnya lumayan banyak nemu kasus lemon atau jeruk'
            ],
            [
                2,
                'Bagaimana cara meminum Perasan Jeruk Nipisnya ?',
                'Ada referensi dari FB nya pak Wied Harry yang bisa menjadi patokan. BANGUN TIDUR: MINUM AIR "JENIPER". Bagi pelaku Food Combining (FC), bangun tidur pagi wajib minum air "jeniper". Air "jeniper" adalah air minum suhu ruang atau suam yang dibubuhi beberapa sendok makan air jeruk nipis atau lemon. ["j', 'Erikar Lebang : Yang dilupakan adalah tata tertib. Banyak pemula foodcombining yang mencoba minum segelas air hangat diberi perasan jeruk nipis atau lemon, kadang melupakan bahwa minum pun mengaktifkan sistem cerna. Lihat dulu, beri otak kesempatan mengirimkan feedback kepada sistem cerna, minum set'
            ],
            [
                2,
                'Jadi apa yang sebaiknya dimakan pada waktu pagi hari ?',
                'Food Combining identik dengan pemanfaatan buah segar sebagai bahan baku makanan untuk sarapan. Sifat buah mudah cerna, ringan, tetapi memberikan asupan energi yang signifikan. Konsumsi sarapan yang sesuai dengan kebutuhan tubuh di fase ini (bukan sesuai kemauan). Pilih substansi makan yang sehat tep'
            ],
            [
                2,
                'Mengapa Buah ? ',
                'Karena tubuh berada di fase pembuangan sisa metabolisme malam. Energi terpakai dan yang akan dipakai sangat besar, jangan diganggu!. Sistem cerna Anda berada dalam kondisi terbaiknya di pagi hari. Beragam enzym cerna juga mudah distimulasi aktif di momen ini.'
            ],
            [
                2,
                'Kapan baiknya buah dikonsumsi ?',
                'Sarapan buah bagi pemula sebaiknya lakukan berkala 06.00 – 11.00.'
            ],
            [
                2,
                'Bagaimana cara mengonsumsi buah ?',
                'Konsumsi buah dalam keadaan perut kosong atau beri jarak 15-20 menit sebelum makan. Sesudah makan sebaiknya tidak menyantap buah, setidaknya 4-5 jam kemudian, hal ini berlaku juga untuk buah yang di jus.'
            ],
            [
                2,
                'Bagaimana cara memakannya ?',
                'Kunyah dengan baik pastikan tercampur air liur. Saat kenyang hentikan makan.'
            ],
            [
                2,
                'Bagaimana dengan jus ?',
                'Sama saja, pastikan tercampur air liur sebelum menelannya.'
            ],
            [
                2,
                'Seperti apakah sebaiknya mengonsumsi buah, di makan langsung atau di buat jus ?',
                'Divariasikan saja antara di jus ataupun dimakan langsung,  tips untuk minum jus, saat minum selipkan ke areal bawah lidah, karena disana tempat kelenjar liur berada, tahan sekitar dua-tiga detik sebelum telan.'
            ],
            [
                2,
                'Bagaimana kalau saya lapar lagi setelah sarapan buah ?',
                'Berdasar pada karakter, buah memang mudah mengenyangkan tapi juga mudah menempatkan kita dalam keadaan lapar kembali. Oleh sebab itu konsumsilah buah secara berkala, terutama untuk pemula. Jangan makan buah sekali saja, bekali diri dgn buah bila beraktivitas. Makanlah setiap terasa lapar. Hingga tib'
            ],
            [
                2,
                'Saya suka kembung, mulas dan pusing sewaktu mengonsumsi buah, kenapa ya ?',
                'Hal tersebut diakibatkan karena mengonsumsi buah secara tergesa-gesa, karena buah tidak tercampur enzim cerna dalam air liur serta lonjakan gula darah yang mendadak.',
            ],
            [
                2,
                'Bagaimana kalau saya tidak sarapan ?',
                'Melewatkan masa sarapan secara rutin akan membuat momen itu terlewat dan secara akumulatif melemahkan kerja sistem cerna. Tentu saja yang jelas adalah menghilangnya kesempatan tubuh dapatkan energi pemula hari. Membuat nafsu makan tidak terkendali.'
            ],
            [
                2,
                'Buah yang bagaimana yang sebaiknya di konsumsi ?',
                'Sebaiknya buah dikonsumsi dalam keadaan matang pohon, berserat serta berair adalah kriteria terbaik. Buah matang pohon memiliki kandungan gula buah yang sederhana dan Enzim cernanya telah lengkap, hingga tdk hrs dicerna oleh lambung. Buah mengkal tidak, menyulitkan sistem cerna.'
            ],
            [
                2,
                'Apakah durian, nangka dan cempedak boleh dimakan untuk sarapan ?',
                'Hindari buah beralkohol seperti durian, nangka dan cempedak karena tidak sesuai dengan kriteria buah baik santap di pagi hari.'
            ],
            [
                2,
                'Amankan mengkonsumsi buah di pagi hari, kan asam dan perut kosong ?',
                'Konsumsi buah dalam keadaan perut kosong tidak menimbulkan masalah bagi asam lambung, karena sifat buah adalah pembentuk basa. Kecuali bila alam bawah sadar telah ada ketakutan tinggi terhadap buah, bisa memicu sakit perut atau biasa disebut psychosomatic.'
            ],
            [
                2,
                'Apakah buah-buahan dari keluarga melon dapat dimakan berbarengan dengan buah lain ?',
                'Kadar air pada keluarga watermelon 90% lebih, itu membuat mereka segera dicerna. Kalo digabung dengan yang lain riskan terfermentasi karena proses cernanya jadi melambat, sehingga sering ditemukan menjadi kembung. Apabila tidak ada masalah bisa dilanjutkan.'
            ],
            [
                2,
                'Bagaimana kalau saya kehabisan stok buah ?',
                'Level kedua sarapan, bila buah tidak ada atau waktu mendekati makan siang adalah ragam sayuran segar, potong atau jus. Kemudian quinoa, oatmeal (non instan non susu) bisa jadi pilihan terakhir.'
            ],
            [
                2,
                'Apakah bengkuang, alpukat, tomat, ketimun, bit adalah buah ?',
                'Termasuk dalam kategori buah yang bersifat sayur karena ber fruktosa rendah.'
            ],
            [
                2,
                'Apakah kurma bisa dikategorikan sebagai buah segar ?',
                'Kurma yang banyak beredar di Indonesia rata-rata adalah manisan kurma yang berfruktosa tinggi. Cari kurma yang masih buah bukan manisan.'
            ],
            [
                2,
                'Bagaimana dengan kombinasi Protein Hewani – Pati ?',
                'Protein Hewani – Pati. Protein Hewani apabila dicampur dengan pati akan menghasilkan ”masalah” bagi pencernaan manusia. Karena masing-masing makanan memiliki enzim berbeda untuk diolah dalam tubuh.'
            ],
            [
                2,
                'Enzim apa saja itu dan apa dampaknya ?',
                'Karbohidrat dicerna oleh Enzim Amilase (terdapat di air liur) dan protein hewani dicerna oleh Enzim Pepsin (bekerja begitu makanan memasuki alat cerna dalam perut). Sayangnya kedua enzim ini tidak bisa bekerja saa bertemu satu sama lain. Amilase akan berhenti bekerja sehingga menghasilkan karbohidra'
            ],
            [
                2,
                'Bagaimana jika dilihat dari sisi waktu cernanya ?',
                'Antara pati dan protein hewani memerlukan waktu yang berbeda untuk terurai. Zat dalam protein hewani cenderung lebih lama terurainya ketimbang karbohidrat.'
            ],
            [
                2,
                'Apa yang terjadi apabila terus menerus seperti itu ?',
                'Paduan makanan tersebut akan menimbulkan semacam endapan sisa yang tak terurai baik oleh tubuh dan akan disimpan dalam usus besar sebagai pusat penyimpanan zat tidak terpakai dalam tubuh manusia. Endapan yang menumpuk akan mengundang bakteri dan parasit yang akan mengganggu kesehatan secara umum.'
            ],
            [
                2,
                'Apakah dengan kondisi seperti ini tubuh tidak dalam kondisi homeostatis ? ',
                'Iya, kondisi tubuh seperti itu akan membuat pH tubuh bergerak ke kutub asam dan bagaikan magnet bagi penyakit.'
            ],
            [
                2,
                'Bagaimana dengan kombinasi Protein – Sayuran ?',
                'Protein - Sayuran. Kombinasi ini ideal dan sangat melengkapi satu sama lain. Karena protein hewani adalah pembentuk asam, sayuran terutama yang segar sangat melengkapi karena sifatnya sebagai pembentuk basa. Konsumsi secara bersamaan akan meminimalisasi pengeruh buruk protein hewani terhadap tubuh.'
            ],
            [
                2,
                'Bagaimana dengan konsumsi sayuran yang tinggi nilai patinya seperti kentang, talas, ubi, jagung dan ',
                'Untuk sayuran jenis ini bukanlah jenis sayuran yang dianjurkan untuk dipadukan dengan protein hewani.'
            ],
            [
                2,
                'Bagaimana dengan Protein Nabati ?',
                'Catatan berbeda diberika kepada protein nabati. Protein ini tergolong netral terutama dalam bentuk pascafermentasi seperti tempe karena ringan dalam mencernanya. Kandungan lemaknya pun tidak memberatkan.'
            ],
            [
                2,
                'Jadi protein nabati boleh dikombinasikan dengan pati ?',
                'Iya'
            ],
            [
                2,
                'Bagaimana dengan kombinasi Pati – Sayuran ?',
                'Pati – Sayuran. Kombinasi ini juga saling melengkapi dan ideal. Serat pada sayuran akan membuat efek buruk pati yang berlebihan sedikit teratasi. Sayuran memberikan rasa kenyang sehingga konsumsi pati dalam jumlah banyak jadi berkurang.'
            ],
            [
                3,
                'Bagaimana tata tertib makan ?',
                'Makan adalah ritual harian yang harus dilakukan secara baik, nikmati waktu makan dan lakukan tata tertib makan, yaitu : 1. Sediakan waktu khusus untuk makan dua puluh lima sampai tiga puluh menit apapun kondisinya. Duduk dengan baik setidaknya membuat kita harus menyediakan waktu dan menyiagakan sel'
            ],
            [
                3,
                'Bagaimana dengan makanan lunak seperti bubur, bagaimana memakannya ?',
                'Jangan tertipu oleh bentuknya yang lembut dan lunak, tetap kunyah dan pastikan tercampur air liur dan telan perlahan.'
            ],
            [
                4,
                'Apakah ada contoh menu Food Combining yang ideal ?',
                'Ada, keadaan ideal dimaksudkan hidangan rumahan atau dana cukup untuk membeli bahan makanan sehat dan berkualitas.'
            ],
            [
                4,
                'Apa saja contohnya Food Combining rumahan?',
                'Contoh menu Food Combining yang ideal. 05.00 Bangun pagi dan minum segelas air hangat berperasan jeruk lemon. 07.00 Sarapan (Jus buatan sendiri tanpa gula). 09.00 Kudapan pagi (buah potong segar). 12.00 Makan siang (Menu Protein) yang terdiri dari Ayam goreng, taoge goreng, lalapan, sayur segar, dan'
            ],
            [
                4,
                'Bagaimana melakukan Food Combining dalam keadaan yang tidak ideal, seperti anak kost, membeli hidang',
                'Bisa, tetap memungkinkan untuk melakukan Food Combining.'
            ],
            [
                4,
                'Apa saja contohnya Food Combining anak kost?',
                'Contoh Food Combining dengan Menu Tidak Ideal. 05.00 Bangun pagi dan minum segelas air hangat berperasan jeruk lemon atau nipis. 07.00 Sarapan buah potong yang sesusai musim agar terjangkau. 09.00 Kudapan pagi berupa Buah potong segar pedagang kaki lima. 12.00 Makan siang berupa Nasi, semur tahu, te'
            ],
            [
                4,
                'Kalau kudapan sore atau malam itu bagaimana ya ?',
                'Kudapan Sore, Apabila makan siang berupa Protein Hewani + Sayuran untuk kudapan sore nya hidangan yang netral yaitu sayuran (Jus sayuran atau salad sayuran). Apabila makan siang protein mudah cerna kudapan sore bisa kudapan pati (bubur kacang hijau atau pisang rebus). Kudapan malam, biasanya sayuran'
            ],
            [
                5,
                'Bagaimana dengan konsumsi air di Food Combining ?',
                'Air sangat penting karena ¾ tubuh kita terdiri atas air. Apabila kekurangan air akan menjadi bencana dalam tubuh dan juga akan membuat pH tubuh mudah sekali bergerak ke sisi asam. Jangan hanya mengandalkan rasa haus.'
            ],
            [
                5,
                'Kenapa bukan pada waktu haus ?',
                'Karena rasa haus adalah alarm tubuh akan kebutuhan air pada level terakhir. Minum sebelum haus menyerang. 8 gelas sehari(atau 6 gelas sehari untuk lansia) adalah kebutuhan standar, apabila beraktivitas berat ataupun sakit harus ditambah.'
            ],
            [
                5,
                'Bagaimana minum air saat berada di waktu makan ?',
                'Beri jeda antara waktu sebelum dan sesudah makan untuk minum. Konsumsi air sekitar 500 ml, satu jam sebelum mengonsumsi makanan. Bisa juga dua - tiga jam sesudah makan. Tetapi di tengah waktu makan sebaiknya jangan atau tidak lebih dari ½ gelas air.'
            ],
            [
                6,
                'Apa pedoman utama bagi pemula yang akan melakukan Food Combining ?',
                'Ada 2 hal penting untuk itu, yaitu : 1. Jangan membuat peraturan sendiri, patuhi juklak makan. 2. Jangan mencoba menggabung-gabungkan dengan pola diet lain tanpa memiliki pedoman pemahaman yang jelas.'
            ],
            [
                6,
                'Apakah Food Combining bisa bikin berat badan saya turun ?',
                'Food Combining adalah pola makan yang bertujuan utama membuat tubuh menjadi sehat. Food Combing bukan pola makan pelangsing atau penambah berat badan. Tubuh sehat akan mencari sendiri berat idealnya.'
            ],
            [
                6,
                'Apakah Food Combining bisa menyembukan sakit saya ?',
                'Penempatan Food Combining sebagai upaya pengobatan spesifik terhadap penyakit adalah salah kaprah. Food Combining adalah pola makan yang membuat tubuh sehat. Tubuh yang telah sehat akan memiliki kemampuan untuk menyembuhkan dirinya sendiri atau mempertahankan diri dari beragam penyakit, ini adalah l'
            ],
            [
                6,
                'Adakah porsi makan dalam Food Combining ?',
                'Tidak ada, porsi makan ditentukan oleh tubuh masing-masing. Food Combining makan harus kenyang Karena #FoodCombining pola makan sehat bukan diet asal langsing, bedakan dengan Diet Kalori. Masing-masing orang porsi makannya bisa berbeda-beda, sesuaikan saja.'
            ],
            [
                6,
                'Apakah saya akan kelaparan bila melakukan Food Combining ?',
                'Yang pasti Food Combining tidak akan membuat kelaparan, kebingungan menghitung kalori, Indeks Glikemik, kandungan protein, lemak dan lain-lain. Tubuh tidak akan didikte sesuai keinginan tapi berorientasi pada kesehatan, mencari berat idealnya sendiri, turun atau naik karena orientasinya adalah keseh'
            ],
            [
                6,
                'Apakah Ibu Hamil, Ibu Menyusui, Balita ataupun Lansia bisa melakukan Food Combining ?',
                'Sangat bisa, malahan lebih baik dilakukan agar mendapatkan tubuh yang selalu sehat. Begitu juga untuk ibu hamil, banyak pelaku Food Combining yang merasakan manfaat dengan melakukan ini, selain berat badan tidak terlalu naik, bayi pun menjadi sehat.'
            ],
            [
                6,
                'Kalau saya sakit apakah tetap ber Food Combining ?',
                'Iya, Harusnya tubuh yang sedang sakit, justru lebih dipatuhi kebutuhannya. Lagipula kedisiplinan dalam hal ini menggambarkan intensitas prioritas kita dalam merawat tubuh.'
            ],
            [
                6,
                'Dalam 1 Minggu apakah Food Combining setiap hari ?',
                'Tidak harus, bisa lakukan 5 hari Food Combining dan 2 hari bisa bebas dari Food Combining, biasa disebut Cheating. Sebaiknya lakukan cheating pada waktu makan siang dan malam, sebaiknya sarapan buah tetap diutamakan.'
            ],
            [
                6,
                'Boleh gak sih gak cheating ?',
                'Cheating itu bisa dilakukan bisa gak, didalam Food Combining. Tetapi gak pernah sama sekali gak semua akan mampu yang ada malah gak bisa awet. Jadi pintar-pintar menyiasati. Trik sebelum melakukan cheating, 1. Lakukan di hari libur, karena untuk pelaku food combining yang sudah bagus saluran cernany'
            ],
            [
                6,
                'Sejak kapan anak-anak bisa ber Food Combining ?',
                'Seumur hidup si anak. Waktu masih menyusui, ibunya yang Food combining, saat ia mulai dikenalkan makanan, ya mulai pergunakan juklak food combining.'
            ],
            [
                6,
                'Bagaimana untuk bayi yang sudah MPASI apakah Food Combining sudah bisa diterapkan ?',
                'Secara logika mendasar #food combining sudah bisa mulai diterapkan, kendalanya mungkin lebih di sisi konsep mengunyah yang belum maksimal. Pemakaian alat penghancur seperti blender dan jus bisa diperbantukan di sini. Silahkan pahami juklak dasar, juga bisa beli bukunya mas Wied Harry yang banyak men'
            ],
            [
                6,
                'Bagaimana dengan penggunaan omega 3 boleh tidak ?',
                'Bisa di minum atau bisa untuk menjadi campuran jus.'
            ],
            [
                6,
                'Bagaimana kalau saya lapar lagi kalau malam-malam ?',
                'Makan itu harus sesuai dengan kebutuhan tubuh dan makan harus kenyang, tidak ada pembatasan porsi makanan asal sesuai juklak. Intinya belajar berkomunikasi dengan tubuh, two way traffic, selama kita makan yang benar, tubuh nanti menentukan sendiri, signal kenyang tentu termasuk juga disini.'
            ],
            [
                6,
                'Saya terkadang lapar lagi setelah makan siang bagaimana ya ?',
                'Waktu cerna atau makan adalah 12.00 - 20.00 perhatikan padu padan pada waktu makan harus sesuai dengan juklak. Secara garis besar apabila kita makan padu padan Pati + Sayur kemudian akan makan Protein Hewani + Sayur beri jarak satu setengah sampai dua jam. Apabila Protein Hewani – Sayur kemudian aka'
            ],
            [
                6,
                'Bagaimana apabila saya sering melakukan olahraga ?',
                'Menurut Erikar Lebang : One small fact about sport yang gak banyak orang tahu. Setiap sesi olahraga meningkatkan jumlah radikal bebas dalam tubuh. Semakin banyak berolahraga, semakin tinggi persentase "molekul cacat" dalam tubuh yang harus "diakomodasi" oleh sistem pertahanan, agar tidak merusak kes'
            ],
            [
                6,
                'Apakah kegunaan tidur selama lima menit setelah makan siang ?',
                'Upaya tubuh mengembalikan kondisi homeostatis. Tidur setelah sepuluh sampai lima belas menit setelah makan selama lima sampai tiga puluh menit.'
            ],
            [
                6,
                'Apakah pola makan tidak sehat bisa membuat badan menjadi moody dan berfikiran negatif ?',
                'Terkait dengan banyak hal yang diakibatkan oleh pola makan buruk itu, misal terkait dengan ketidakseimbangan hormon dan ketidakseimbangan flora usus (penyebab candidiasis, misalnya), disamping sebab2 lainnya.'
            ],
            [
                6,
                'Saya pemula terkadang masih khawatir makanan tidak sesuai dengan juklak, bagaimana ya ?',
                'Menurut Erikar Lebang : Gak usah terlalu direpotkan oleh detil, bila Anda seorang pemula. Kesalahan kecil pasti terjadi disana-sini, itu yang kita perbaiki sesuai dengan perjalanan waktu. Belasan tahun saya melakukan Food combining, pengetahuan dan aplikasi saya membaik mungkin di sekitar tahun ke-5'
            ],
            [
                6,
                'Saya gak suka sayur apalagi yang mentah, jus sayur juga, gimana ya ?',
                'Menurut Erikar Lebang : Kalau mau sehat, ya harus konsekuen. Kalau mau konsekuan, tentu penghalalan bersifat subyektif seperti "aduh gak suka", "wah kayaknya gak bisa", "ih, rasanya kayak apa ya?" atau rengekan sejenis, harus dibuang jauh-jauh. Dan berpikir secara jelas dan rasional, tapi ini semau '
            ],
            [
                6,
                'Bagaimana kalau kerjanya sistem shift, misal shift malam untuk jenipernya bagaimana ya ?',
                'Menurut Erikar Lebang : Kalau memang sudah keharusan hidupnya berlawanan dengan kodrat alami tubuh, pekerjaan semisal, ya mau gimana lagi, namanya juga cari duit. Cuma sudah tahu siklus hidup kebalik-balik, ya pola makannya dijaga sebaik mungkin. Jangan udah tau hidup malam, malah minum kopi bergela'
            ],
            [
                6,
                'Food Combining juga memakai rujukan buku the miracle of enzim ya. Kalau di buku itu pola makan masih',
                'Menurut Erikar Lebang : Gak juga, kalau bacanya teliti. Hiromi Shinya sangat menyarankan konsumsi protein hewani sesedikit mungkin. Tidak sekalipun ia menyarankan mengkonsumsi protein hewani berat apalagi bersamaan dengan karbohidrat dalam jumlah banyak. Makanan seperti steak semisal, hanya disarank'
            ],
            [
                6,
                'Saya pecandu kopi bagaimana ya ?',
                'Menurut Erikar Lebang : Walau mengganti kopi dengan cairan masuk 3 kali lipat, tetap saja perusakan kopi masih terjadi. Penghambatan tubuh memanfaatkan mineral dalam tulang sulit menyerap kalsium semisal, ketergantungan otak terhadap kafein -adiksi yang perlahan menurunkan kemampuannya, pengerasan p'
            ],
            [
                6,
                'Infused Water',
                'Menurut Erikar Lebang : Beberapa jenis buah bergula tinggi bisa larut dalam air, terminum. Ini ganggu sistem cerna. Kalau mau buat sifat basa lemah pada air minum, coba pergunakan buah kuat sifat sayurnya, seperti irisan lemon.'
            ],
            [
                6,
                'Cadangan Enzim',
                'Salah satu teori terkenal terkait persediaan cadangan enzim dalam tubuh kita. Itu menjadi penjelas kenapa ada orang yang bisa seenaknya hidup, tapi gak pernah sakit-sakit. Karena tubuhnya punya cadangan enzim yang banyak, sehingga bisa diboroskan untuk menetralisir kebiasaan buruknya. Tapi apa yang '
            ],
            [
                6,
                'Bagaimana dengan MPASI untuk anak ?',
                'Di ajari pengenalan makanan dan tercukupi kebutuhan tubuhnya jangan sampai picky eater. ASI tetap wajib di berikan sampai selesai. Jangan sampai di biarkan lapar. Orang tua nya juga harus pandai-pandai mengakali agar anaknya mau makan. Pada dasarnya mau pakai cara food combining atau yang lain. Oran'
            ],
            [
                6,
                'Bagaimana cara mengukur asam basa dalam tubuh ?',
                'Menurut Erikar Lebang : Ada sih BGA (Blood Gas Arterial) atau kadang disebut juga ABG Anak baru ganteng #eh maksudnya alhamdulillah saya gantengnya udah lama #eh lagi. Anu maksudnya, tes pengambilan darah arteri. Tapi ini tes yang tidak umum dilakukan pada pasien biasa, bahkan pasien sakit sekalipun'
            ],
            [
                6,
                'Boleh gak mengkonsumsi air dingin ?',
                'Kalo suhu dinginnya terlalu jauh sama tubuh, akan mengurangi kinerja enzim. Makanya kalo bikin Jeniper disarankan pake air hangat, karena air hangat mengaktifkan enzim dalam tubuh. Hal serupa juga dilakukan oleh tubuh kita saat terserang penyakit. Tubuh mengaktifkan "demam" agar suhu badan naik, seh'
            ],
            [
                7,
                'Bagaimana apabila harga buah di daerah saya mahal, sehingga saya hanya bisa makan buah yang hampir s',
                'Menurut Erikar Lebang : Idealnya memang kita tidak mengkonsumsi makanan itu-itu saja. Tapi dalam kasus ini, toh sudah ada kesadaran untuk menambahkan buah lain "bervariasi" sebagai pelengkap, dalam porsi lebih kecil. Saya kira ini langkah positif dan upaya "meet the halfway" yang cukup signifikan. S'
            ],
            [
                7,
                'Di supermarket sering lihat kalau di stiker buah itu ada kodenya, artinya apa ya ?',
                'Berdasarkan artikel yang ada di Kompasiana, berikut artinya : Empat angka, misal 4016 : konvensional, tidak organic. Lima angka dimulai angka 8: konvensional, tidak organic & modifikasi genetis (GMO). Lima angka dimulai angka 9: Organik. Catatan : Konvensional artinya ditanam dengan pupuk berbasis p'
            ],
            [
                7,
                'Apabila saya bangun tidur 3 pagi dan langsung beraktivitas apakah langsung Jeniper ?',
                'Iya, karena jeniper di konsumsi pada saat mulut dalam keadaan kering. Apabila tidur kembali bisa saja dikonsumsi di bangun yang kedua.'
            ],
            [
                7,
                'Mengapa pada waktu sarapan buah yang di jus terasa mual ?',
                'Umumnya karena dikonsumsi terlalu cepat, akibatnya air liur tidak bisa menyentuh materi yang seharusnya "diproses" oleh enzim cerna dalam liur. Itu penyakit umum orang menjus sesuatu. Selama diminumnya tertib. Perlahan, ditelan seteguk-seteguk, selipkan di bawah lidah, agar tercampur air liur'
            ],
            [
                7,
                'Bagaimana dengan Yoghurt apakah aman di konsumsi ?',
                'Menurut Erikar Lebang : Ada beberapa pendekatan tentang yoghurt. Proses fermentasinya telah memecah kasein (protein susu) menjadi lebih moderat dan diterima tubuh. Demikian juga dengan kandungan laktosa (gula susu). Dengan kata lain itu sebab yoghurt adalah turunan susu yang cenderung aman dan bergu'
            ],
            [
                7,
                'Berapa sih perbandingan air dengan buah atau sayur pada waktu membuat jus ?',
                'Rata-rata sepertiga volume yang akan di blender, bisa di lihat juga di manual blendernya.'
            ],
            [
                7,
                'Apakah boleh memblender dengan menggunakan es batu ?',
                'Suhu terlalu dingin & terlalu rendah dibanding suhu tubuh membuat enzim makanan segar menjadi pasif.'
            ],
            [
                7,
                'Mengapa buah yang masuk keluarga melon harus dikonsumsi eksklusif ?',
                'Karena keluarga melon cepat sekali melakukan pembusukan dibanding buah lain nya. Konsumsi keluarga melon terlebih dahulu, baru menyusul buah lain 15 menit kemudian. Apabila bersamaan ada juga yang mengalami kembung, ada juga yang tidak. Baiknya dikonsumsi terpisah.'
            ],
            [
                7,
                'Apakah oatmeal bisa dikonsumsi untuk sarapan ?',
                'Apabila itu oatmeal instan pabrikan apalagi terkadang mengandung susu, maka konsumsi sesekali rekreasional. Konsumsi raw oatmeal yg tidak mengandung susu itu yang baik. Menyantap oatmeal yang pasti jauhkan dari waktu santap buah, sesuai siklus tubuh pagi, havermut tergolong unsur yang cenderung bera'
            ],
            [
                7,
                'Kalau kelapa muda bagaimana ?',
                'Jadikan sebagai minuman rekreasional karena air yang terbaik hanya air putih.'
            ],
            [
                7,
                'Bagaimana pemakaian bawang putih ke dalam dressing ?',
                'Diparut sedikit saja di atas sayur agar tidak terlalu terasa baunya.'
            ],
            [
                7,
                'Apakah boleh mengkonsumsi mayonaise untuk dressing ?',
                'Sedikit saja dan perbandingan porsi dressing dan sayur harus lebih banyak sayur, namun Lebih hati2 dengan mayo. Mungkin kita tidak sadar sering "ngeributin" soal garam/gula refinasi dan lain-lain. Atau bahkan "sekedar" tomat yang jadi asam karena diblansir sebentar, tapi kadang lupa dengan mayo dala'
            ],
            [
                7,
                'Berapa lama sebaiknya sayur disimpan didalam kulkas ?',
                'Kalau bicara kualitas maksimal, sejatinya sayuran segar yang baik, sebaiknya tidak melebihi 3 hari waktu simpan dalam lemari pendingin.'
            ],
            [
                7,
                'Apakah ikan dan telur termasuk Protein Hewani ?',
                'Iya'
            ],
            [
                7,
                'Bagaimana persentase protein hewani didalam Food Combining ?',
                'Menurut Erikar Lebang : Kalau mengacu pada data klinis usus sehat Hiromi Shinya, malah cuma 15% perhari, bisa perminggu, kalau gak perbulan, bahkan pertahun. Pelaku #FoodCombining meminjam istilah pak Wied Harry adalah "minimal menjadi vegan setengah hari, karena pagi sampai siang, pasti cuma makan '
            ],
            [
                7,
                'Lalu bagaimana sebaiknya konsumsi protein hewani ?',
                'Menurut Erikar Lebang : Lebih baik saya menjawab dengan "protein hewani bukan makanan yang cocok dikonsumsi rutin oleh manusia".'
            ],
            [
                7,
                'Bagaimana dengan terasi ?',
                'Terasi termasuk makanan prosesan berbasis hewani. Kalo sekedar buat penambah rasa dalam jumlah dikit banget ga masalah dikombinasi sama sayuran pati. Ga sampe seperempat sendok teh udah bikin rasa saus jadi beda.'
            ],
            [
                7,
                'Bolehkah daun singkong di konsumsi mentah ?',
                'Tidak, daun singkong memang bisa memberi semacam efek sianida bila dikonsumsi dalam keadaan mentah, reaksi pusing, sesak nafas, mual, diare.'
            ],
            [
                7,
                'Bagaimana apabila saya muntah setelah makan malam, apakah saya harus makan lagi ?',
                'Kembali pada prinsip, buah tidak bisa menggantikan makan malam. Lebih baik konsumsi jus sayur sederhana seperti mentimun atau wortel, bila waktunya muntah terjadi mendekati waktu tidur, minum perlahan-lahan, pastikan tercampur air liur. Bila waktu muntah masih dalam jam makan malam, makan seperti bi'
            ],
            [
                7,
                'Apa perbedaan beras merah, coklat, hitam dan putih', 'Beras Coklat : hanya dihilangkan sekamnya saja dan tidak dipoles hingga menjadi beras putih. Efeknya masih terdapat kandungan nutrisi pada beras coklat yang dibutuhkan oleh tubuh. Beras Putih : merupakan proses selanjutnya dari beras coklat dan bisa dibilang sudah ga ada gunanya bagi tubuh, selain itu dikutip dari buku bang Erikar Lebang : Beras putih tanpa melalui proses khusus umumnya akan melalui produk yang masih kasar,suram,berkulit keras dan sulit dimasak pulen. Untuk menghindari hal "tidak mengenakkan" menghadirkan serangkaian proses dan penambahan bahan yg membuat beras putih menjadi baha'
            ],
            [
                7,
                'Bagaimana cara memasak nasi merah ala resep dari Hiromi Shinya ?',
                'Dia memberi resep beras merah diberi segenggam ikan teri, biji-bijian aneka rupa, sedikit raw oat, sejumput garam laut organik. Harus ikan teri, paling tidak ikan yang wujudnya kecil. Karena kita bisa mengkonsumsinya dari kepala sampai ke ekor. Memberikan sumbangan kalsium bagi tubuh. Ini pun jumlah'
            ],
            [
                7,
                'Bagaimana cara membuat beras berkecambah ?',
                'Setelah dicuci air bersih, rendam dengan air matang hangat. Jangan terlalu penuh biar berasnya tetap bisa bernapas. Setelah air rendaman dingin, masukkan kulkas minimal selama seharian. Sebelum dimasak, keluarkan dulu dari kulkas & biarkan sebentar sampai mendekati suhu ruang (Diambil dari resep Shi'
            ],
            [
                7,
                'Bagaimana apabila makan dengan kuah daging dan dibarengi dengan pati, berapa banyak paling banyak bo',
                'Menurut Andang Gunawan : Paling banyak 3 sendok makan apabila dibarengi dengan pati. Patuhi sesuai juklak.'
            ],
            [
                7,
                'Apakah boleh makan sayuran segar dengan dressing kacang seperti gado-gado atau pecel ?',
                'Bisa, secukupnya saja porsinya untuk sekedar membantu proses makan sayur segar.'
            ],
            [
                7,
                'Bagaimana dengan penggunaan santan ?',
                'Menurut resep dari pak Wied Harry disarikan oleh Mohammad Zainal Arief. Santan boleh asal ga berlebihan dan yang terpenting tanpa dipanaskan. Setelah parutan kelapa diperas dengan air matang hangat (bukan panas), perasan santannya dipisahkan. Waktu mau makan tinggal ditambahkan beberapa sendok ke at'
            ],
            [
                7,
                'Bagaimana dengan mie ?',
                'Mie basis buatnya saja sudah bukan makanan ideal yang layak untuk digolongkan sebagai makanan sehat, konsumsi sesekali untuk rekreasional. Karena termasuk bahan prosesan dan terbuat dari tepung (pati) dan telur (hewani) tidak masuk kedalam juklak Food Combining.'
            ],
            [
                7,
                'Bagaimana dengan penggunaan ragi ?',
                'Ragi adalah salah satu mimpi buruk bagi keseimbangan bakteri dalam sistem cerna. Minimnya bakteri baik akan membuat terganggungya produksi enzim, vitamin seperti B12 dan banyak elemen penting sistem cerna lain. Dan dominasi bakteri jahat, juga akan merusak banyak hal yang seharusnya dimanfaatkan tub'
            ],
            [
                7,
                'Kalau tempe beragi bagaimana ?',
                'Pada tempe beda konsep raginya, ia berupa kapang, kalo gak salah namanya rhizopus, gunanya menyederhanakan substansi pada kedelai agar lebih mudah dicerna. Protein pada kedelai. Kata ragi pada tempe itu hanya istilah. Sementara pada produk tepung-tepungan, pemakaian ragi berbeda maksud dan tujuannya'
            ],
            [
                7,
                'Apakah sayuran pengganti kalsium susu ?',
                'Sayur-sayurang yang renyah kebanyakan juga tinggi kalsium. Contoh sayuran berkalsium : Brokoli, Pumpkin Seeds, Bok Choy, Okra, Collards (Sejenis Sawi/Caisim), Kale, Turnip Greens, Alpukat, Butternut Squash, Almonds, Green Beans (Seperti Edamame), Daging Kelapa, Khoirabi, Brazil Nuts, Artichoke, Baya'
            ],
            [
                7,
                'Kalau keju bagaimana ya ?',
                'Menurut Erikar Lebang : Yah, kalau cuma sekedar ditaburi doang sih, selama sayurannya seabreg, kejunya gak akan sempat memberi efek buruk bagi tubuh. Lain kalau kejunya berjumlah banyak, kebanyakan gitu. Apalagi tumpukan keju ditaburi sayur.'
            ],
            [
                7,
                'Bagaimana dengan jagung ?',
                'Jagung termasuk golongan pati sehingga harus dimasak untuk mengkonsumsinya. Selain itu membrannya keras dan sulit di cerna tubuh sehingga pastikan dikunyah halus.'
            ],
            [
                7,
                'Bagaimana dengan cacing yang suka ada di sayuran ?',
                'Menurut Erikar Lebang : Trauma cacing pada sayuran segar itu ketakutan berlebihan. PH tubuh yang netral dan cenderung basa, homeostasis, akan menyulitkan cacing hidup leluasa dalam sistem cerna Jadi biji pepaya itu bukan obat dalam konteks keharusan. Lebih sekedar langkah pencegahan. Apalagi kalau d'
            ],
            [
                7,
                'Bagaimana dengan pestisida yang ada di sayuran ?',
                'Mencuci buah pakai cuka apel. Rendam dalam baskom kecil, tetesin tiga tetes cuka apel. Biasanya airnya akan berubah warna. Pada dasarnya tubuh yang sehat akan menghilangkannya.'
            ],
            [
                7,
                'Apakah ketimun boleh dimakan kalau saya darah rendah ?',
                'Menurut Erikar Lebang : Ketimbang takut sama beginian. Saya malah lebih menakuti, teh, kopi, susu, roti, daging-dagingan dan lain sebagainya yang umum dimakan penderita darah rendah tanpa rasa was-was.'
            ],
            [
                7,
                'Bagaimana dengan ikan laut apakah aman dikonsumsi ?',
                'Menurut Erikar Lebang : Kita harus berhati-hati terhadap ikan laut. Karena sifat predatornya, semakin besar ikannya, semakin banyak ia berpotensi mengkonsumsi ikan lain. Maksudnya, ia berpotensi juga membawa efek buruk yang dari tubuh ikan lain, dalam hal ini mercuri, semisal. Ada juga yang mengangg'
            ],
            [
                7,
                'Bagaimana menyiasati jus sayur yang langu ?',
                'Menurut Erikar Lebang : Untuk jus sayur memang suka gak suka kendala, bahasa jermannya, "bau langu", masih sering merebak. Contohnya beet root, waduh itu bau langunya "kenceng" bener. Apalagi daunnya, mengutip kata sahabat saya Kiki Purliageng "udah kayak makan tanah!". Gula pada sayuran juga umumny'
            ],
            [
                7,
                'Gimana sih milih semangka atau melon yang bagus ?',
                'Semangka melon ditepuk2. Yg getarannya kerasa keras sampe bawah kadar airnya msh tinggi artinya msh segar. Trus lihat bekas patahan batangnya. Pilih yg msh segar (baru patah). ? Mohammad Zainal Arief ?'
            ],
            [
                7,
                'Kalau kelapa muda boleh dimakan buat sarapan gak ya ?',
                'Kelapa muda hampir sama kayak siwalan. Sering disebut buah tapi karena fruktosanya rendah (<5%), jadi kurang ideal dimakan bareng buah.'
            ],
            [
                7,
                'Bagaimana dengan jamur ?',
                'Konsumsi jamur sesekali saja, jangan terus menerus.'
            ],
            [
                7,
                'Bagaimana dengan sayuran yang disiram air panas saja ?',
                'Menurut Erikar Lebang : Enzim mati di suhu 45-48 derajad celcius, dalam hitungan detik sekalipun. Hitungannya detik, jadi potensi merusaknya jelas tetap ada. Selain sayuran rusak, bakteri biasanya malah gak kenapa-kenapa. Faktanya membunuh bakteri secara agresif kadang malah lebih sulit ketimbang me'
            ],
            [
                7,
                'Kenapa sih harus makan sayur mentah ?',
                'Diambil dari buku bu Andang Gunawan : Food Combining Kombinasi Makanan Serasi. Setiap makanan sebetulnya sudah mengandung enzim dan co-factor (vitamin dan mineral yang berhubungan dengan enzim) yang fungsinya untuk menguraikan molekul-molekulnya sendiri. Perubahan makanan dari mentah sampai matang d'
            ],
            [
                7,
                'Bagaimana cara memilih alpukat ?',
                'https://www.facebook.com/photo.php?fbid=10152266463221665&set=gm.477367659029433&type=1'
            ],
            [
                7,
                'Bagaimana dengan melinjo ?',
                'Menurut Erikar Lebang : Kalau biji melinjo umumnya dikonsumsi dalam bentuk emping, yang prosesnya saja sudah kelewat panjang, ya bisa jadi beri masalah. Tapi bila dikonsumsi seperti sayur asem misal, gak terlalu masalah. Makannya juga gak bisa banyak. Cuma kalau sudah punya problem asam urat, ya pro'
            ],
            [
                7,
                'Bagaimana dengan madu ?',
                'Konsumsi sesekali atau sesedikit mungkin karena indeks glikemiknya kan tidak rendah. Kalau mengkonsumsinya sembarangan, minum 1-2 sendok makan sekaligus, kadang sampai 3 kali sehari, seperti yang saya sering lihat dilakukan oleh banyak orang. Otomatis gula darahnya akan melonjak. Ya tahu sendiri kal'
            ],
            [
                7,
                'Bagaimana dengan acar ?',
                'Sayuran segar lebih baik. Kalau sudah gak segar, either dianggap rekreasional atau paling gak bukan dianggap sebagai substansi menyehatkan.'
            ],
            [
                7,
                'Apa sih Sea Salt itu dan apa gunanya ?',
                'Sea Salt adalah garam laut asli dalam bentuk terbaik sesuai kebutuhan tubuh, bukan garam refinasi seperti yang umumnya dijual di pasar. Makanya (dalam pemakaian normal) penderita darah tinggi yang biasanya menghindari garam, saat mempergunakan sea salt, mereka tidak menemukan masalah, malah berfungs'
            ],
            [
                8,
                'Bagaimana dengan yang Insomnia ?',
                'Yang sering tidak disadari pemicu insomnia itu sendiri hadir rutin dalam kehidupan keseharian kita dan bersembunyi dalam beragam bentuk. Mulai teh, kopi, protein hewani, makanan prosesan, dan banyak lagi. Hingga ke cara makan yang salah. Insomnia sering menjadi cara tubuh untuk memberikan alarm kepa'
            ],
            [
                8,
                'Bagaimana hubungan Food Combining dengan Penyakit Jantung ?',
                'Menurut Erikar Lebang : Penyakit jantung ada banyak ragam. Kalau yang dimaksud adalah penyumbatan koroner, saya pernah membuat kultwit tentang kenapa pembuluh darah merapuh dan menggetas sehingga perlu "ditebalkan" oleh tubuh, yang akumulasinya berujung pada penyumbatan. Sesederhana itu foodcombinin'
            ],
            [
                8,
                'Apakah berarti plak nya dulu dihilangkan baru FC ?', 'Menurut Erikar Lebang : Justru disitu salahnya pendekatan dunia kesehatan konvensional. Plak yang sebenarnya langkah defensif tubuh mengatasi pembuluh darah rapuh, malah dianggap musuh dan berusaha "dibuang" paksa. Makanya gagal melulu. Selama pembuluh darahnya rapuh, tumpukan plak akan "diadaan" te'
            ],
            [
                8,
                'Bagaimana dengan hasil trigliserida yang masih tetap tinggi walaupun sudah melakukan FC ?',
                'Menurut Erikar Lebang : Salah satu kelebihan dan sekaligus kelemahan #FoodCombining yang paling umum adalah longgarnya jendela prinsip dasar. Itu harus diakui. Logika dasar bolehnya mengkonsumsi karbohidrat selama tidak dibarengi protein hewani semisal, memang mamaksimalkan kerja sistem cerna, terka'
            ],
            [
                8,
                'Mengapa pada saat awal melakukan FC sering terasa pusing ya ?',
                'Tidak apa-apa itu adalah proses penyembuhan atau healing crisis dari pola makan yang tidak benar selama ini. (Ibu Andang Gunawan) Healing crisis itu kalau bahasa medisnya withdrawal symptoms atau gejala putus obat alias sakaw. Bisa pusing/mual/pilek/gatal2/jerawatan/gelisah/kedinginan. Setiap orang '
            ],
            [
                8,
                'Adakah hubungan Food Combining dengan Toksoplasma ?',
                'Tidak, bisa dilihat di link berikut http://storyofnotes.tumblr.com/post/50642386316/sumber-penyebar-toksoplasma-kibulansehat-by#notes'
            ],
            [
                8,
                'Apakah FC bisa menyembuhkan Asam Urat ?',
                'Menurut Weni Dassir Nurlaela : Bisa, dibuku bang Erikar makanan mentah justru meringankan kerja ginjal, membuang asam urat berlebihan : " Wied Harry mengamati secara empiris para pasien asam urat yang ditemui memiliki karaktek kesalahan yang khas. Mereka yang mengkonsumsi sayuran pada umumnya melaku Menurut Erikar Lebang : Saya lebih membahas ke sisi kenapa asam urat acap diidentikkan kepada sayuran? Karena umumnya ahli kesehatan konvensional memiliki pengetahuan tentang makan sehat yang jauh dari baik. Jadi bila bertemu satu masalah kesehatan, dan dikaitkan dengan makanan, kebanyakan mereka cu'
            ],
            [
                8,
                'Tentang diabetes', 'Menurut Erikar Lebang : Kalau pankreasnya sudah rusak parah, pilihan terbaik adalah dengan membatasi gula dalam ragam bentuk, even fructose. Buah sedang gula seperti apel, jambu air, pir adalah pilihan populer. Yang dilupakan: penderita diabetes, sering didoktrin menghindari gula sedemikian rupa. Ta Menurut Andang Gunawan : Hindari semua refined carbohydrate, kalau bisa hindari juga semua produk hewani terutama yang tinggi lemak, pakai minyak seperlunya. Hindari makanan yang mengandung lemak trans. Sebetulnya untuk diabetes nomor satu minimize makanan tinggi lemak. Makanan karbohidrat dikontrol'
            ],
            [
                8,
                'Saya belum mencoba FC tetapi saat ini saya sudah 1 minggu tidak BAB, bagaimana ya apa yang harus dim',
                'Menurut Erikar Lebang : Pada dasarnya #FoodCombining adalah pemahaman kesehatan general, dimana "pola makan benar" mengambil porsi sangat besar. Karena basisnya "pemahaman harus diutamakan", saya jarang sekali menjawab pertanyaan terkait contoh menu makan. Selain itu pemakaian makanan rendah guna pe'
            ],
            [
                8,
                'Bagaimana dengan penderita suatu penyakit ?',
                'Tetap lakukan Food Combining sesuai juklak dan diperbanyak sayuran segar. Yang harus diingat sepiring lalapan penuh, kadang hanya bisa menjadi "pengkompensasi" kalau yang dimakan juga sepiring nasi putih dan ditambah makanan prosesan lain. Jadi sebelum berguna untuk tubuh, habis terlebih dahulu oleh'
            ],
            [
                8,
                'Bagaimana dengan pendapat penyakit keturunan ?',
                'Menurut Erikar Lebang : Penyakit terkait genetik itu persentasenya kecil sekali. Kazuo Murakami profesor madya, Vanderbilt University, ia berhasil mengurai kode genetik manusia, dia bilang kurang lebih begini, "Manusia memang secara genetik bisa lemah di satu sisi, tapi perubahan sifat gen-nya menja'
            ],
            [
                8,
                'Bagaimana dengan ambeien ?',
                'Menurut Erikar Lebang : Ambeien atau radang hemorrhoid internal-eksternal. Salah satu penyebab paling populer yang sering dituding adalah faktor genetika, salah mengejan dan lain lain. Tapi lucunya yang jarang dituding justru malah hal yang paling signifikan dalam menyumbang kemungkinan penyebab ter'
            ],
            [
                8,
                'Bagaimana pandangan tentang obat asam lambung ?',
                'Menurut Erikar Lebang : Antasida, obat penahan asam lambung, yang umum dipakai untuk problem lambung. Yang dirusak pertama justru bukan ginjal, tapi lambungnya sendiri. Kemampuan lambung mengeluarkan asam, terus menerus ditekan oleh antasida yang bersifat basa. Padahal asam lambung itu banyak sekali'
            ],
            [
                8,
                'Makanan apa ya yang bisa menyembuhkan sakit saya ?',
                'Menurut Erikar Lebang : "Konsep konsumsi "sesuatu untuk" atau "sesuatu karena" itu masih terjebak dalam kerangka berpikir #foodtherapy bukan #FoodCombining. Berlebihan mengkonsumsi "air kelapa agar.." atau menyalahkan "karena alpukat.." itu adalah contohnya. Yang saya maksud memahami bahwa makan seh'
            ],
            [
                8,
                'Pada saat minum jeniper gigi suka ngilu itu kenapa ya ?',
                'Drg Prima Kusuma Wardhani : Mungkin giginya over sensitif karena emailnya tipis atau ada gigi yang berlubang kecil. Bisa dicek ke dokter gigi dulu, untuk mensiasati supaya saat minum jeniper ga ngilu karena kena gigi langsung, cara minumnya bisa pake sedotan biar langsung masuk ketenggorokan, ga nye'
            ],
            [
                8,
                'Saya punya penyakit asam urat, setelah rutin FC sewaktu saya cek asam urat belum normal ya ?',
                'Erikar Lebang - "Buat food diary aja, tulis apapun yang dimakan. Jadi kalau asam uratnya naik, perhatikan warna sayur yang dikonsumsi, kalau cenderung terlalu banyak yang hijau, ragamkan warnanya, apalagi kalau sayurnya itu-itu saja. Yang lebih krusial lagi adalah jumlah konsumsi air. Jangan terpaku'
            ],
            [
                8,
                'Bagaimana dengan konsumsi makanan tertentu untuk penyakit tertentu ?',
                'Nah , Konsep konsumsi "sesuatu untuk" atau "sesuatu karena" itu masih terjebak dalam kerangka berpikir food therapy bukan #FoodCombining. Berlebihan mengkonsumsi "air kelapa agar.." atau menyalahkan "karena alpukat.." itu adalah contohnya. Yang saya maksud memahami bahwa makan sehat adalah bagian da'
            ]
        ];

        foreach ($arrayFaq as $faq) {
            $faqObj = new Faq();
            $faqObj->pertanyaan = $faq[1];
            $faqObj->jawaban = $faq[2];
            $faqObj->category_faq_id = \App\CategoryFaq::where('id', $faq[0])->first()->id;
            $faqObj->save();
        }
    }
}