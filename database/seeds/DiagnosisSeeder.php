<?php

use Illuminate\Database\Seeder;

class DiagnosisSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gArr = [
            'maag',
            'gelisah',
            'lelah',
            'sembelit',
            'bab',
            'nafsu makan tidak terkendali',
            'sakit kepala',
            'sakit kepala',
            'mual',
            'kembung',
            'flu',
            'nyeri otot',
            'gangguan kulit',
            'gangguan emosi',
            'kedinginan',
            'perubahan warna urin',
            'bau nafas',
            'alergi',
            'sensitif',
            'lonjakan gula',
            'sariawan',
            'obesitas',
            'migren',
            'perut sebelah',
            'muntah',
            'sendawa'
        ];

        foreach ($gArr as $g) {
            $newG = new \App\Gejala();
            $newG->nama = $g;
            $newG->save();
        }

        $penyebab = [
            [
                'cheating',
                '7,22,8,23,24'
            ],
            ['mengunyah makanan',
                '7,22,8,9,25,26'
            ],
            [
                'detoksifikasi',
                '7,22,8,9,4,5,10,11,12,13,14,15,16'
            ],
            [
                'waktu makan',
                '1,2,3,4,5,6'
            ],
            [
                'kombinasi makanan',
                '1,2,3,4,5,17,18,19,20,21'
            ]
        ];

        foreach ($penyebab as $p) {
            $newP = new \App\Penyebab();
            $newP->nama = $p[0];
            $newP->solusi = "Solusi sementara";
            $newP->save();
            $idxs = explode(',', $p[1]);
            $newP->gejalas()->sync($idxs);
        }
    }
}